default: all

all: add commit

add:
	git add -A

commit:
	git commit -m "${m}"
