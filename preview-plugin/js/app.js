// App.js contains all Ember.js modules for Azul Mobile Beta
//
// Author: Azul Frontend
// Version: 1.0
// Date: 12/8/2013


/***************************************
	App Constant's & Global Objects
***************************************/
var server = "http://128.83.196.226:2048"; // Web server
var deals = []; // List of deals from search query result

var merchantserver = "http://128.83.52.199:8001/";

/***************************************
	App Configuration
***************************************/
App = Ember.Application.create({
	currentPath: '', // current state 
	previousPath: '', // previous state
	browse_demo: true

});

App.ApplicationController = Ember.Controller.extend({
	currentUser: null,

	/* updateCurrentPath() method updates current & previous state variables
	 * @observes - change in state
	*/
	updateCurrentPath: function() {
		App.set('previousPath', App.get('currentPath'));
        App.set('currentPath', this.get('currentPath'));
        console.log("prev path = " + App.get('previousPath'));
        console.log("current path = " + App.get('currentPath'));
    }.observes('currentPath'),
});

/***************************************
	App Router
***************************************/
App.Router.map(function() {
	this.resource('loading');
	this.resource('browse');
	this.resource('moreinfo', { path: '/more_info/:deal_id' });
	this.resource('purchase', { path: '/purchase/:deal_id' });
});

/***************************************
	Index
***************************************/
App.IndexRoute = Ember.Route.extend({
	// redirect() method redirects App on load
	redirect: function() {
		this.transitionTo('loading');
  	},
});

App.LoadingRoute = Ember.Route.extend({
	model: function(){
		return deals;
	},
});

App.LoadingView = Ember.View.extend({
	templateName: 'loading',
	// Event listener for AZUL logo touch and logout tile
	getDeals: function(self){
		var date_time = new Date();
		console.log("GETTING DEALS");
		// Search query
		var search_data = {
			"type":"deal",
			"data":{
				"search_query": "Tacos", // search text
				"user_id": -99,
				"date_time": rfc3339_dt(date_time),
				"location": {
					"lat": 30.2500,
					"lon": -97.7500,
				},
				"distance": 20,
				"price": 20
			}
		};
		var posted = $.post(server + "/search", search_data); // Post to search 
		posted.done(function(searchRes){
			// Update Globals
			console.log(searchRes);
			// TODO read cash and append to deals array
			var cashed_deal = $.get('http://128.83.52.246:2048/merchants/0/dealpreview', function(data) {
				console.log('DATA');
				console.log(data);
				var cashed_deal = data;
				console.log(cashed_deal);
				setTimeout(
					function(){
						if(cashed_deal){
							cashed_deal = parseCash(cashed_deal);
							console.log(cashed_deal);
							deals[0] = cashed_deal // Get cash deal
							deals = parseDeals(deals);
						}else{
							console.log('unsuccessful in getting data');
						}
						deals.push.apply(deals, parseDeals(searchRes.results)); // Add search deals
						setTimeout(function(){
							self.get('controller').transitionToRoute('browse');
						}, 2000);
					}
				, 2000);
			});
		});
		posted.fail(function(res) {
			alert("Search not working, please retry");
		});
	},

	// document.ready for search state
	didInsertElement: function() {
		this.getDeals(this);
	}
});

/***************************************
	Navigation Header
***************************************/
App.HeaderView =  Ember.View.extend({
	classNames: ['header-view'],
	//Event listener for the side menus
	menuListeners: function(){
		// Declare variables
		var self = this;
		var menu_profile_trigger = $('#profile-trigger'); //left main menu trigger

		// Event handler for "menu icon" trigger in header
		menu_profile_trigger.hammer().on('touch', function(event){
			alert("No menu in preview mode");
		});
	},
	// Update AZUL logo when current state == search
	updateIcons: function(self){
		if(App.get('currentPath') == "browse"){
			$('#home-trigger').removeClass('logo-back');
		}
	},
	// Event listener for AZUL logo touch and logout tile
	touchListeners: function(self){
		var thing = self.get('controller');
		$('#home-trigger').hammer().on('touch', function(event){
			thing.transitionToRoute("browse");
		})
	},

	// document.ready for search state
	didInsertElement: function() {
		this.updateIcons(this);
		this.menuListeners();
		this.touchListeners(this);
	}
});


/***************************************
	Browse Module
***************************************/

// View for browse navigation parent view
App.BrowseView = Ember.View.extend({
	touchListeners: function(self){
		// Declare variables
		var favorite_btn = $('#browse-footer-favorite-trigger');
		var sort_btn = $('#browse-footer-sort-trigger');
		var map_btn = $('#browse-footer-map-trigger');

		// Event listener for favorite icon
		favorite_btn.hammer().on('touch', function(event){
			var thing = $(this).children('.favorite-icon');
			if(!self.get('controller.firstTime')){
				var flag = !self.get('controller.favFlag');
				self.set('controller.favFlag', flag);
				self.set('controller.sortFlag', false);
				self.set('controller.mapFlag', false);
				if(flag){
					thing.css('content', 'url("img/icon/favorite_w.png")');
					sort_btn.children('.icon').css('content', 'url("img/icon/sort.png")');
					map_btn.children('.icon').css('content', 'url("img/icon/map.png")');
				}else{
					thing.css('content', 'url("img/icon/favorite.png")');
				}
				self.set('controller.content', deals);
			}
		});

		// Event listener for sort icon
		sort_btn.hammer().on('touch', function(event){
			var thing = $(this).children('.sort-icon');
			if(!self.get('controller.firstTime')){
				var flag = !self.get('controller.sortFlag');
				self.set('controller.favFlag', false);
				self.set('controller.sortFlag', flag);
				self.set('controller.mapFlag', false);
				if(flag){
					thing.css('content', 'url("img/icon/sort_w.png")');
					favorite_btn.children('.icon').css('content', 'url("img/icon/favorite.png")');
					map_btn.children('.icon').css('content', 'url("img/icon/map.png")');
				}else{
					thing.css('content', 'url("img/icon/sort.png")');
				}
				self.set('controller.content', deals);
			}
		});

		// Event listener for map icon
		map_btn.hammer().on('touch', function(event){
			var thing = $(this).children('.map-icon');
			if(!self.get('controller.firstTime')){
				var flag = !self.get('controller.mapFlag');
				self.set('controller.favFlag', false);
				self.set('controller.sortFlag', false);
				self.set('controller.mapFlag', flag);
				if(flag){
					thing.css('content', 'url("img/icon/map_w.png")');
					favorite_btn.children('.icon').css('content', 'url("img/icon/favorite.png")');
					sort_btn.children('.icon').css('content', 'url("img/icon/sort.png")');
				}else{
					thing.css('content', 'url("img/icon/map.png")');
				}
				self.set('controller.content', deals);
			}
		});
	},
	// updateNavigation checks state when reloading browsedeals and changes navigation accordingly
	updateNavigation: function(self){
		if(self.get("controller.favFlag")){
			$('#browse-footer-favorite-trigger').children('.icon').css('content', 'url("img/icon/favorite_w.png")');
		}else if(self.get("controller.sortFlag")){
			$('#browse-footer-sort-trigger').children('.icon').css('content', 'url("img/icon/sort_w.png")');
		}else if(self.get("controller.mapFlag")){
			$('#browse-footer-map-trigger').children('.icon').css('content', 'url("img/icon/map_w.png")');
		}
	},
	
	// updateModel resets all flags
	updateModel: function(self){
		self.set('controller.favFlag', false);
		self.set('controller.sortFlag', false);
		self.set('controller.mapFlag', false);
	},
	// document.ready for browse navigation
	didInsertElement: function(){
		this.updateModel(this);
		this.updateNavigation(this);
		this.touchListeners(this);
	}
});

// View for browse tiles list
App.BrowsetilesView = Ember.View.extend({
	classNames: ['tileView'],
	itemController: 'item',
	dragListeners: function(self){
		// Delcare variables
		var window_width = $('html').width();
		var body = $('.contain');
		var speed = 0;

		// Event handler for deal space interaction
		$('.deal-space').hammer({tap_max_touchtime: 150}).on("tap dragleft dragright dragend", function(event){
			var thing = $(this);
			if(body.position().left == 0){
				switch(event.type){
					case 'tap':
						console.log(event.target.id);
						if( (event.target.id != "fav-btn-trigger")&& (event.target.id != "buy-btn") && (event.target.id != "fav-btn")){
							console.log("yippe");
							window.location = merchantserver + '#/more_info/' + thing.attr('deal_id');
						}
						else if((event.target.id == "fav-btn-trigger")||(event.target.id == "fav-btn")){
							// Else it is a favorite
							var id = $(this).attr('deal_id');
							var deal = deals.findBy('deal_id', id);
							var fav = Ember.get(deal, 'favorite');
							if(!fav){
								$(this).find('#fav-btn').attr("src",'img/btn/fav_btn_thick.png');
							}
							else{
								$(this).find('#fav-btn').attr("src",'img/btn/fav_btn.png');
							}
							Ember.set(deal, 'favorite', !fav);
							if(self.get('controller.favFlag')){
								$(this).parent('.deal-tile').remove();
							}

						}
						else if(event.target.id == 'buy-btn'){
							window.location = merchantserver + '#/purchase/' + thing.attr('deal_id');
						}
						break;
					case 'dragleft':
					case 'dragright':
						var delta = event.gesture.deltaX;
						speed = event.gesture.velocityX;
						body.bind("touchmove", function(e){e.preventDefault();}); // Prevent body from scrolling vertically
						thing.css("-webkit-transform", 'translate3d('+ delta +'px, 0px, 0px)');
						break;
					case 'dragend':
						var midScreen = window_width/2; // Midpoint theshold
						var offsets = $(this).offset();
						var direc = event.gesture.direction;
						var deleteSibling = thing.siblings( ".delete");
						body.unbind("touchmove");
						thing.addClass('tile-slide-fast');
						if( ((speed > 0.5)&&(direc == "right")) || (offsets.left > midScreen) ){
							thing.css('-webkit-transform', 'translate3d('+(window_width)+'px, 0px, 0px)');
							setTimeout(function(){
								thing.removeClass('tile-slide-fast');
							}, 450);
							deleteSibling.addClass('fadeIn');
						}
						else if( ((speed > 0.5)&&(direc == "left")) || (offsets.left < (-1*midScreen)) ){
							thing.css('-webkit-transform', 'translate3d('+(-1*window_width)+'px, 0px, 0px)');
							setTimeout(function(){
								thing.removeClass('tile-slide-fast');
							}, 450);
							deleteSibling.addClass('fadeIn');
						}
						else {
							thing.removeClass('tile-slide-fast');
							thing.css('-webkit-transform', 'translate3d(0px, 0px, 0px)');
						}
						break;
					}
				}
		});

		// Event handler for on release of "undo" button
		$('.undo-span').hammer().on("release", function(event){
			// Declare variables
			var thing = $(this);
			var tile = thing.parent().siblings( ".deal-space" );

			// If menu !open, undo
			if(body.position().left == 0){
				tile.addClass('tile-slide-slow');
				thing.parent().removeClass('fadeIn');
				setTimeout(function(){
					tile.removeClass('tile-slide-slow');
				}, 500);
				tile.css('-webkit-transform', 'translate3d(0px, 0px, 0px)');
			}
		});

		// Event handler for on release of "delete" button
		$('.delete-button').hammer().on('release', function(event){
			// Declare variables
			var tile = $(this).parent().parent(); // Get the tile associated with this delete button
			var deal = deals.findBy('deal_id', $(this).parent().siblings('.deal-space').attr('deal_id'));
			var index = deals.indexOf(deal);

			deals = deals.slice(0, index).concat(deals.slice(index+1, deals.length)); // Remove deal

			// If menu !open, delete
			if(body.position().left == 0){
				setTimeout(function(){
					tile.remove();
					console.log(deals.filterBy('favorite').length);
					if( (deals.filterBy('favorite').length  == 0) && (self.get('controller.favFlag')) ) {
						self.set('controller.model', deals);
					}
					else if(deals.length == 0){
						self.set('controller.model', deals);
					}
				}, 400);
			}
		});

	},
// browse demo function
	demo: function(self){
		// Declare variables
		var demo_tile = $('.deal-space:eq(1)');
		var timeouts = [];
		var deleteInterval;
		// Clear all time intervals
        for (var i = 1; i < 9999; i++){
       		window.clearInterval(i);
       	}
       	// Hide DOM elements
       	$('#dot-slide-delete').hide();
       	$('#dot-slide-delete-undo').hide();

       	// If model > length 1, start swipe animation
       	if(self.get('controller.model').length > 1){
			// Animation for swip to delete
			var startBrowseInfo = setTimeout(function(){
				swipeDeleteDemo();
				deleteInterval = setInterval(swipeDeleteDemo, 5250);
			}, 100);
			function swipeDeleteDemo(){
				$('#dot-slide-delete').show();
				$('#dot-slide-delete-undo').hide();
				$('#dot-slide-delete').parent().addClass('dotSwipeToDeleteDot');
				demo_tile.addClass('swipeToDeleteTile');
				timeouts = [];

				timeouts.push(setTimeout(function(){
					demo_tile.css('-webkit-transform', 'translate3d(100%,0,0)');
				}, 250));
				timeouts.push(setTimeout(function(){
					$('#dot-slide-delete').hide();
				}, 500));
				timeouts.push(setTimeout(function(){
					demo_tile.siblings('.delete').addClass('fadeIn');
				}, 1250));
				timeouts.push(setTimeout(function(){
					$('#dot-slide-delete-undo').show();
					$('#dot-slide-delete').parent().removeClass('dotSwipeToDeleteDot');
				}, 2750));
				timeouts.push(setTimeout(function(){
					$('#dot-slide-delete-undo').hide();
					demo_tile.css('-webkit-transform' ,'translate3d(0,0,0)');
				}, 3250));
				timeouts.push(setTimeout(function(){
					demo_tile.siblings('.delete').removeClass('fadeIn');
				}, 4250));
			}
		}
		// Event handler for demo screen
		$('.demo-contain').hammer().on('touch', function(event){
			clearTimeout(startBrowseInfo);
			clearInterval(deleteInterval);
			for(var i = 0, z = timeouts.length; i < z; i++){
       			clearTimeout(timeouts[i]);
       		}
			demo_tile.removeClass('swipeToDeleteTile');
			demo_tile.siblings('.delete').removeClass('fadeIn fadeOut');
			setTimeout(function(){
				demo_tile.css('-webkit-transform' ,'translate3d(0,0,0)');
			},100);
			$(this).css('opacity', 0);
			setTimeout(function(){
				self.set('controller.firstTime', false);
				App.set('browse_demo', false);
				self.dragListeners(self);
			},500);
		});
	},
	// setStars() method updates rating stars
	setStars: function(){
		$('span.stars-gray').stars();
	},

	
	// setFavs updates all favorite deals
	setFavs:function(self){
		var deals = self.get("controller.model");
		$('.deal-space').each(function(){
			var id = $(this).attr('deal_id');
			var deal = deals.findBy('deal_id', id);
			if(deal.favorite){
				$(this).find('#fav-btn').attr("src",'img/btn/fav_btn_thick.png');
			}
			else{
				$(this).find('#fav-btn').attr("src",'img/btn/fav_btn.png');
			}
		});
	},
	// renderDeals renders list of deals & otherMerchants to DOM
	renderDeals: function(self){
		// Declare variables
		var html = '';
		var temp_deals = [];
		// If sort flag, render sort deals
		if(self.get('controller.sortFlag')){
			temp_deals = deals.slice(0);
			temp_deals.sort(function(a,b){return a.price_now - b.price_now;});
		} else if(self.get('controller.favFlag')){
			for(var x = 0; x < deals.length; x++){
				if(Ember.get(deals[x], 'favorite')){
					temp_deals.push(deals[x]);
				}
			}
		} else {
			temp_deals = deals.slice(0);
		}
		for(var x = 0; x < temp_deals.length; x++){
			html += '<div class="col-xs-12 clear tile deal-tile"><div class="delete"><div class="col-xs-4 clear delete-span undo-span"><p class="text-beige text-large">Undo</p></div><div class="col-xs-8 clear delete-span delete-button"><p class="text-beige text-large text-right">Delete</p></div></div><div class="deal-space bg-beige" deal_id="'+Ember.get(temp_deals[x], 'deal_id')+'"><div class="col-xs-4 clear deal-tile-img-div"><img src="'+Ember.get(temp_deals[x], 'img')+'"class="deal-tile-img"></div><div class="col-xs-8 clear deal-tile-info voffset2-5"><div class="col-xs-12 clear deal-tile-info-price"><span class="text-gray clear text-large avenir-light">$'+Ember.get(temp_deals[x], 'price_reg')+' for <span class="price-prev text-orange">$'+Ember.get(temp_deals[x], 'pricePrev')+'<span class="text-xsmall cents">.'+Ember.get(temp_deals[x], 'pricePrevCents')+'</span></span><span class="price-now text-orange"> $'+Ember.get(temp_deals[x], 'priceNow')+'<span class="text-xsmall cents">.'+Ember.get(temp_deals[x], 'priceNowCents')+'</span></span></span></div></div><div class="col-xs-8 clear deal-tile-info"><div class="col-xs-9 clear deal-tile-info-text"><span class="text-uppercase text-gray text-xsmall clear merchant-name">'+Ember.get(temp_deals[x], 'merchant')+'</span><br/><div class="text-gray text-xsmall clear categories">'+Ember.get(temp_deals[x], 'category')+'</div><span class="stars-gray" rating="'+Ember.get(temp_deals[x], 'rating')+'"></span><br/><span class="text-gray text-xsmall clear">'+Ember.get(temp_deals[x], 'distance')+' mi</span></div><div class="col-xs-3 clear deal-tile-buttons"><div class="text-center" id="fav-btn-trigger"><img class="fith25 icon favorite-icon" id="fav-btn" src="img/btn/fav_btn.png"></div><div class="col-xs-12 clear"><button type="button" class="btn button-rounded text-large" id="buy-btn">BUY</button></div></div></div></div></div>';
		}
	
		$('.tileView').append(html);
		// Update deal space inventory
		$('.deal-space').each(function(){
			var deal = deals.findBy('deal_id', $(this).attr('deal_id'));
			var inventory = Ember.get(deal, 'inventory');
			var buy_btn = $(this).find('#buy-btn');
			$(this).parent('.deal-tile').css('-webkit-transform', 'translate3d(0px,0px,0px)'); // translate for animations
			// Update inventory
			if(inventory > 5){
				buy_btn.addClass('button-green');
			}else if(inventory >= 3){
				buy_btn.addClass('button-yellow');
			}else{
				buy_btn.addClass('button-red');
			}
		});
		// Update merchant font size
		$('.merchant-name').each(function(){
			var merchant_name = new String($(this).text());
			var num_words = merchant_name.split(" ").length;
			var word_length = merchant_name.length;
			if( (num_words == 2) && (word_length > 15) ){
				$(this).removeClass('text-medium');
				$(this).addClass('text-small');
			}else if( (num_words == 3) && (word_length > 18)){
				$(this).removeClass('text-medium');
				$(this).addClass('text-small');
			}else if (num_words >=4){
				$(this).removeClass('text-medium');
				$(this).addClass('text-small');
			}
		})
	},
	// document.ready for browse tiles view
	didInsertElement: function(){
		this.renderDeals(this);
		this.setStars();
		this.setFavs(this);
		if(App.get('browse_demo')){
			this.demo(this);
		}
		else{
			this.dragListeners(this);
		}
	}
});

App.BrowseRoute = Ember.Route.extend({
	model: function(){
		return deals;
	},
	setupController: function(controller, model) {
	    controller.set('model', deals);
	}
});

App.BrowseController = Ember.ArrayController.extend({
	itemController: 'item',
	sortProperties: ['merchant'],
  	sortAscending: true,
	mapFlag: false,
	favFlag: false,
	sortFlag: false,
	firstTime: true,
	// True if user favorites something
	hasFavorite: function(){
		return !(this.get('model').filterBy('favorite').get('length') > 0);
	}.property('@each.favorite'),
});

App.ItemController = Ember.ObjectController.extend({
	isFavorite: function(){
		return this.get('favorite');
	}.property('favorite'),
});

/***************************************
	Browse Map
***************************************/
App.BrowsemapView = Ember.View.extend({
	classNames: ['mapView'],
	// Adds each deal location to the map view
	addMap: function(map){
		for(var x=0; x<deals.length; x++){
			var lat = deals[x].location.lat;
			var lon = deals[x].location.lon;
			console.log("merchant = " + deals[x].merchant);
			console.log("lat = " + lat + ", lon = " + lon);
			var marker = new L.Marker([lat, lon]).addTo(map);
			marker.bindPopup('<div class="col-xs-5 clear map-img-div"><img src="'+ deals[x].img +'" class="deal-tile-img"></div><div class="col-xs-7 clear"><p class="text-center text-small clear"><b>'+ deals[x].merchant+'</b></p><span class="col-xs-12 text-center text-orange text-xlarge clear">$'+ deals[x].priceNow+'<span class="text-small cents">.'+deals[x].priceNowCents+'</span></span></div>', {maxWidth:150, minWidth:150});
		}
		return;
	},
	// doument.ready for browse map module
	didInsertElement: function(){
		var map = L.map('map'); // Create leaflet map
		var lat = 30.279739;
		var lon = -97.742699;
		var user; // User marker that represents user location
		var userIcon = L.icon({
			iconUrl: 'img/userMap.png',
			iconSize:     [26, 75], // size of the icon
			iconAnchor:   [13, 75], // point of the icon which will correspond to marker's location
			shadowAnchor: [4, 62],  // the same for the shadow
			popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
		});

		// CLoudmade tile layer: http://{s}.tile.cloudmade.com/69d480cad34b43ada9b470555196029e/997/256/{z}/{x}/{y}.png
		// OSM tile layer: http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png
		L.tileLayer('http://{s}.tile.cloudmade.com/69d480cad34b43ada9b470555196029e/997/256/{z}/{x}/{y}.png', {
			attribution: '',
			maxZoom: 18,
			minZoom: 4
		}).addTo(map);
		user = L.marker([lat, lon], {icon: userIcon}).addTo(map);
      		map.setView(new L.LatLng(lat, lon),14); // Set view to location with zoom 14
		this.addMap(map);
	}
});


/***************************************
	More Info Module
***************************************/
App.MoreinfoRoute = Ember.Route.extend({
	model: function(params){
		return deals.findBy('deal_id', params.deal_id);
	}
});

App.MoreinfoController = Ember.ObjectController.extend({
	isMapOpen: false, // True if map is open
	dealsGreen: function(){
		var num = parseInt(this.get('remaining'));
		if(num > 10){
			return true;
		}
		return false;
	}.property('remaining'),
	dealsYellow: function(){
		var num = parseInt(this.get('remaining'));
		if( (num <= 10) && (num >= 5) ){
			return true;
		}
		return false;
	}.property('remaining'),
});

App.MoreinfoView =  Ember.View.extend({
	touchListeners: function(self){
		// Event handler for "map icon" touch
		$('.map-icon').hammer().on('touch', function(event){
			var isMap = !self.get('controller.isMapOpen');
			self.set('controller.isMapOpen', isMap);
			// If map is open, hide image and show map, else reverse
			if(isMap){
				$('.deal-tile-img').hide();
				$('.moreinfo-map').show();
				$('#map-icon').attr('src', "img/icon/map_w.png");
			}
			else{
				$('.deal-tile-img').show();
				$('.moreinfo-map').hide();
				$('#map-icon').attr('src', "img/icon/map.png");
			}
		});
		// Event handler for "fav icon" touch
		$('#fav-btn').hammer().on('touch', function(event){
			var deal = self.get('controller.model');
			var fav = Ember.get(deal, 'favorite');
			if(!fav){
				$(this).attr("src",'img/icon/favorite_w.png');
			}
			else{
				$(this).attr("src",'img/icon/favorite.png');
			}
			Ember.set(deal, 'favorite', !fav);
		});
		// Event handler for "buy btn" touch
		$('#buy-btn').hammer().on('touch', function(event){
			var thing = $(this);
			window.location = merchantserver + '#/purchase/' + thing.attr('deal_id');
		});
		// Event handler for "back icon" navigation touch
		$('.back-icon').hammer().on('touch release', function(event){
			var thing = $(this).children('.icon');
			switch(event.type){
				case 'touch':
					thing.attr('src', "img/icon/back_w.png");
					break;
				case 'release':
					self.get('controller').transitionToRoute('browse');
					thing.attr('src', "img/icon/back.png");
			}
		});
	},
	// updateIcons() updates icons on document.ready
	updateIcons: function(self){
		if(self.get('controller.model.favorite')){
			$('#fav-btn').attr("src",'img/icon/favorite_w.png');
		}
	},
	// document.ready for more info module
	didInsertElement: function(){
		$('span.stars-beige').stars();
		var map = L.map('map');
		var lat = this.get('controller.model.location.lat');
		var lon = this.get('controller.model.location.lon');
		L.tileLayer('http://{s}.tile.cloudmade.com/69d480cad34b43ada9b470555196029e/997/256/{z}/{x}/{y}.png', {
			attribution: '',
			maxZoom: 18,
			minZoom: 4
		}).addTo(map);
		var marker = new L.Marker([lat, lon]).addTo(map);
		map.setView(new L.LatLng(lat, lon),14);
		$('.moreinfo-map').hide();
		this.updateIcons(this);
		this.touchListeners(this);
	}
});

/***************************************
	Purchase Module
***************************************/
App.PurchaseView = Ember.View.extend({

	// touchListeners for purchase view
	touchListeners: function(self){
	
		// Touch listener for purchase button
		$('#purchase-trigger').hammer().on('touch', function(event){
			alert("Cannot purchase in preview mode");		
		});

	},
	// document.ready for purchase state
	didInsertElement: function(){
		this.touchListeners(this);
	}
});
App.PurchaseRoute = Ember.Route.extend({
	model: function(params){
		console.log(params);
		return deals.findBy('deal_id', params.deal_id);
	},
});
App.PurchaseController = Ember.ObjectController.extend({
	totalprice: function(){
		return this.get('model.price_now').toFixed(2);
	}.property('price_now'),
});
