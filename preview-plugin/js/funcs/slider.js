function make_time_text(){
	var html = '';
	var time = new Date();
	time.setMinutes(round_to_thirty(time.getMinutes()));
	for(var i = 0; i < 48; i++){
		html += '<strong id="slider-indicator-time-' + encode_time(time) + '" class="text-medium absolute slider-indicator-time-text">' + twelve_hour_time(time) + '</strong>\n';
		time.setMinutes(time.getMinutes() + 30);
	}
	return html;
}

function make_price_text(){
	var html = '';
	var price = 5;
	for(var price = 5; price <= 25; price += 5){
		html += '<strong id="slider-indicator-price-' + price + '" price="' + price + '" class="text-medium absolute slider-indicator-price-text">$' + price + ' VALUE</strong>';
	}
	return html;
}

function get_time_text(){
	var arr = [];
	var time = new Date();
	time.setMinutes(round_to_thirty(time.getMinutes()));
	for(var i = 0; i < 48; i++){
		arr.push($('#slider-indicator-time-' + encode_time(time)));
		time.setMinutes(time.getMinutes() + 30);
	}
	return arr;
}

function get_price_text(){
	var arr = [];
	for(var price = 5; price <= 25; price += 5){
		arr.push($('#slider-indicator-price-' + price));
	}
	return arr;
}

function show_indicator_text(to_show, to_hide){
	to_hide.css('visibility', 'hidden');
	to_show.css('visibility', 'visible');
}

function encode_time(js_date){
	return js_date.getHours() + '-' + leading_zero(js_date.getMinutes());
}

function get_selector_time(sel){
    var time_arr = ((sel.attr('id')).split('-').slice(3));
    var date = new Date();
    date.setHours(parseInt(time_arr[0]));
    date.setMinutes(parseInt(time_arr[1]));
    date.setSeconds(0);
    return date;
}
