//
//  UIColor+ColorScheme.m
//  Azul
//
//  Created by Tim on 12/19/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import "UIColor+ColorScheme.h"

@implementation UIColor (ColorScheme)
+ (UIColor *)bluePalette{return [UIColor colorWithRed:(47 / 255.0) green:(85 / 255.0) blue:(130 / 255.0) alpha:1];}
+ (UIColor *)darkBluePalette{return [UIColor colorWithRed:(23 / 255.0) green:(53 / 255.0) blue:(81 / 255.0) alpha:1];}
+ (UIColor *)grayPalette{return [UIColor colorWithRed:(127 / 255.0) green:(113 / 255.0) blue:(118 / 255.0) alpha:1];}
+ (UIColor *)darkGrayPalette{return [UIColor colorWithRed:(44 / 255.0) green:(42 / 255.0) blue:(46 / 255.0) alpha:1];}
+ (UIColor *)orangePalette{return [UIColor colorWithRed:(230 / 255.0) green:(96 / 255.0) blue:(39 / 255.0) alpha:1];}
+ (UIColor *)beigePalette{return [UIColor colorWithRed:(247 / 255.0) green:(245 / 255.0) blue:(234 / 255.0) alpha:1];}
@end
