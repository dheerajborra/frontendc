//
//  UIColor+ColorScheme.h
//  Azul
//
//  Created by Tim on 12/19/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ColorScheme)
    + (UIColor *)bluePalette;
    + (UIColor *)darkBluePalette;
    + (UIColor *)grayPalette;
    + (UIColor *)darkGrayPalette;
    + (UIColor *)orangePalette;
    + (UIColor *)beigePalette;
@end
