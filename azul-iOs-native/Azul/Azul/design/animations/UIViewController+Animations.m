//
//  UIViewController+Animations.m
//  Azul
//
//  Created by Tim on 12/20/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import "UIViewController+Animations.h"

@implementation UIViewController (Animations)


/** Animates UIView shift up/down in order to fix keyboard overlay problem
 * @param textField - the text field selected
 * @param up - the direction of the animation
 */
- (void) animateTextField: (UITextField*)textField up: (BOOL)up
{
    const int movementDistance = [[AZLUtils screenLayout] keyboardHeight]; // move up hieght of keyboard
    float movementDuration; // if keyboard up, set time to be slower
    if(up){
        movementDuration = 0.30f; // native keyboard is slower moving up than down
    }else{
        movementDuration = 0.285;
    }
    int movement = (up? -movementDistance : movementDistance);
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

@end
