//
//  UIViewController+Animations.h
//  Azul
//
//  Created by Tim on 12/20/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Animations)

- (void) animateTextField: (UITextField*)textField up: (BOOL)up;

@end
