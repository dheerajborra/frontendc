//
//  AZLFontConfig.h
//  Azul
//
//  Created by Tim on 12/22/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#define FONT_S 12
#define FONT_M 16
#define FONT_L 20
#define FONT_XL 24

