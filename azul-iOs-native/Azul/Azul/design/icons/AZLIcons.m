//
//  AZLIcons.m
//  Azul
//
//  Created by Tim on 12/22/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import "AZLIcons.h"

@implementation AZLIcons

NSString *iconAdd = @"add.png";
NSString *iconBack = @"back.png";
NSString *iconDelete = @"delete.png";
NSString *iconFavorite = @"favorite.png";
NSString *iconForward = @"forward.png";
NSString *iconMap = @"map.png";
NSString *iconMenu = @"menu.png";
NSString *iconShare =  @"share.png";
NSString *iconSort = @"sort.png";

@end
