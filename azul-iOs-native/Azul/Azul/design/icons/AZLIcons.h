//
//  AZLIcons.h
//  Azul
//
//  Created by Tim on 12/22/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AZLIcons : NSObject

extern NSString *iconAdd;
extern NSString *iconBack;
extern NSString *iconDelete;
extern NSString *iconFavorite;
extern NSString *iconForward;
extern NSString *iconMap;
extern NSString *iconMenu;
extern NSString *iconShare;
extern NSString *iconSort;

@end
