//
//  AZLBottomNavigationBar.h
//  Azul
//
//  *Custom bottom icon bar used in pages such as moreinfo, login, etc.
//
//  Created by Tim on 12/21/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AZLIconButton.h"

@interface AZLBottomNavigationBar : UIView{
    int height;
    AZLIconButton *buttonLeft;
    AZLIconButton *buttonMiddle;
    AZLIconButton *buttonRight;
    UIView *leftView;
    UIView *middleView;
    UIView *rightView;
}

@property int height;
@property int viewWidth;
@property (retain) AZLIconButton *buttonLeft;
@property (retain) AZLIconButton *buttonMiddle;
@property (retain) AZLIconButton *buttonRight;

- (id)initWithButtons:(NSString *)left middle:(NSString *)middle right:(NSString *)right color:(UIColor *)color;

@end
