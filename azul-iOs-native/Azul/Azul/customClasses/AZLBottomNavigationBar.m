//
//  AZLBottomNavigationBar.m
//  Azul
//
//  Created by Tim on 12/21/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import "AZLBottomNavigationBar.h"

#define BOTTOMBAR_HEIGHT 55;

@implementation AZLBottomNavigationBar

@synthesize height;
@synthesize viewWidth;
@synthesize buttonLeft;
@synthesize buttonRight;
@synthesize buttonMiddle;

/** Initialize Bottom Nav Bar given 3 icons from left to right (@see AZLIcons.h)
 * @param left - left icon url | nil
 * @param middle - middle icon url | nil
 * @param right - right icon url | nil
 *
 */
- (id)initWithButtons:(NSString *)left middle:(NSString *)middle right:(NSString *)right color:(UIColor *)color
{
    self = [super init];
    if (self) {
        // Initialization code
        [self setBackgroundColor:color];
        [self layoutSubviews];
        // Add Buttons
        buttonLeft = [self addButton:left view:leftView];
        buttonMiddle = [self addButton:middle view:middleView];
        buttonRight = [self addButton:right view:rightView];
        [self addSubview:leftView];[self addSubview:middleView];[self addSubview:rightView];
    }
    return self;
}

/** Layout 3 subview containers left to right
 *
 */
- (void)layoutSubviews
{
    self.height = BOTTOMBAR_HEIGHT;
    self.viewWidth = ([[AZLUtils screenLayout]screenWidth]-30) / 3;
    [self setFrame:CGRectMake(0, [[AZLUtils screenLayout]screenHeight]-self.height, [[AZLUtils screenLayout]screenWidth], self.height)];
    leftView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, viewWidth, self.height)];
    middleView = [[UIView alloc] initWithFrame:CGRectMake(viewWidth+15, 0, viewWidth, self.height)];
    rightView = [[UIView alloc] initWithFrame:CGRectMake(2*viewWidth+15, 0, viewWidth, self.height)];
}

- (AZLIconButton *)addButton:(NSString *)position view:(UIView *)view
{
    AZLIconButton *button;
    if (position != nil) {
        button = [[AZLIconButton alloc] initWithImage:position height:self.height width:self.viewWidth];
        [view addSubview:button];
    }
    return button;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
