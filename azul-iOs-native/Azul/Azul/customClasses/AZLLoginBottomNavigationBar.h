//
//  AZLLoginBottomNavigationBar.h
//  Azul
//
//  Created by Tim on 12/21/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AZLBottomNavigationBar : UINavigationBar{
    UIButton *buttonLeft;
    UIButton *forwardButton;
}

@end
