//
//  AZLLoginTextField.h
//  Azul
//
//  Created by Tim on 12/20/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AZLLoginTextField : UITextField

- (void)initializeParams;

@end
