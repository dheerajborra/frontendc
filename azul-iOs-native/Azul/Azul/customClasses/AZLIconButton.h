//
//  AZLIconButton.h
//  Azul
//
//  *Custom UIButton for Azul icons
//
//  Created by Tim on 12/21/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AZLIconButton : UIButton{
    NSString *iconName;
    UIImage *iconImage;
    UIImage *iconHighlightedImage;
    UIImageView *iconView;
}

@property NSString *iconName;
@property UIImage *iconImage;
@property UIImage *iconHighlightedImage;
@property UIImageView *iconView;

- (id)initWithImage:(NSString *)imageURL height:(int)height width:(int)width;

@end
