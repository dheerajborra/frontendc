//
//  AZLIconButton.m
//  Azul
//
//  Created by Tim on 12/21/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import "AZLIconButton.h"

#define ICONSIZE 46

@implementation AZLIconButton

@synthesize iconName;
@synthesize iconImage, iconHighlightedImage;
@synthesize iconView;

/** Initialize iconButton given icon url
 * @param imageURL - icon URL (@see AZLIcons.h)
 */
- (id)initWithImage:(NSString *)imageURL height:(int)height width:(int)width
{
    self = [super initWithFrame:CGRectMake(0, 0, width, height)];
    if (self) {
        iconName = imageURL;
        [self initializeParams]; // Initialization parameters
        [self addSubview:iconView];
        [self initializeListeners]; // Add icon listeners
    }
    return self;
}

/** Initialize parameters for AZLIconButton
 */
- (void)initializeParams
{
    iconImage = [UIImage imageNamed:iconName];
    iconView = [[UIImageView alloc] initWithFrame:CGRectMake([AZLLayoutConfig centerHorizontally:self widthOfObject:ICONSIZE], 0, ICONSIZE, ICONSIZE)];
    [iconView setImage:iconImage];
    iconHighlightedImage = [UIImage imageNamed:[NSMutableString iconHighlightUrl:iconName]];
}

/** Initialize button listeners for AZLIconButton
 */
- (void)initializeListeners
{
    [self addTarget:self action:@selector(buttonTouchDown:) forControlEvents:UIControlEventTouchDown|UIControlEventTouchDownRepeat|UIControlEventTouchDragEnter];
    [self addTarget:self action:@selector(buttonTouchUp:) forControlEvents:UIControlEventTouchUpInside|UIControlEventTouchUpOutside];
}

/** Event handler for icon button touch down
 * @param selector - selector thats called
*/
- (void)buttonTouchDown:(id)selector{
    [iconView setImage:iconHighlightedImage];
}

/** Event handler for icon button touch up
 * @param selector - selector thats called
 */
- (void)buttonTouchUp:(id)selector{
    [iconView setImage:iconImage];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
