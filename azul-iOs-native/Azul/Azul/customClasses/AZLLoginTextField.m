//
//  AZLLoginTextField.m
//  Azul
//
//  Created by Tim on 12/20/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import "AZLLoginTextField.h"
#import "UIViewController+Animations.h"


@implementation AZLLoginTextField

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self initializeParams];
    }
    return self;
}

/** Initialization specific code
 *
 */
-  (void)initializeParams
{
    [self setFont:[UIFont fontWithName:@"Avenir-Light" size:FONT_L]];
    self.leftViewMode = UITextFieldViewModeAlways;
    self.textColor = [UIColor grayPalette];
    self.borderStyle = UITextBorderStyleNone;
    self.backgroundColor = [UIColor beigePalette];
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    self.returnKeyType = UIReturnKeyDone;
}

/** Overrides parent class which returns drawn bounds for UItextfield
 * @return - rectangle bounds of AZLLoginTextField
 */
- (CGRect)textRectForBounds:(CGRect)bounds
{
    // Add left padding to text box
    return CGRectMake(bounds.origin.x + 30, bounds.origin.y,
                      bounds.size.width, bounds.size.height);
}
/** Overrides parent class which return editable text field for UItextfield
 * @return - rectangle bounds of AZLLoginTextField
 */
- (CGRect)editingRectForBounds:(CGRect)bounds
{
    return [self textRectForBounds:bounds];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
