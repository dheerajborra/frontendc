//
//  AZLAppDelegate.h
//  Azul
//
//  Created by Tim on 12/19/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AZLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
