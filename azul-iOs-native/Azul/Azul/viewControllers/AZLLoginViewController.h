//
//  AZLLoginViewController.h
//  Azul
//
//  **  Parent class for AZLCurrentuser and AZLNewUser View Controllers
//      Includes commonalities between current and new user views
//
//  Created by Tim on 12/31/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AZLLoginTextField.h"
#import "AZLBottomNavigationBar.h"
#import <FacebookSDK/FacebookSDK.h>

@interface AZLLoginViewController : UIViewController <UITextFieldDelegate, FBLoginViewDelegate>{
    IBOutlet UIView *inputView;
    FBLoginView *fbLoginView;
    AZLBottomNavigationBar *footer;
}

- (void)FbLoginInitWithYPos: (int)yPos;
- (void)addViews;

@end
