//
//  AZLRegisterViewController.m
//  Azul
//
//  Created by Tim on 12/19/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import "AZLNewUserViewController.h"
#import "UIViewController+Animations.h"

@interface AZLNewUserViewController ()

@end

@implementation AZLNewUserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeView];
	// Do any additional setup after loading the view.
    firstName.delegate = self;
    lastName.delegate = self;
    email.delegate = self;
    password.delegate = self;
}

- (void)initializeView
{
    self.view.backgroundColor = [UIColor bluePalette]; // Change background color
    // Initialize parameters
    inputView.frame = CGRectMake(0, 250, [[AZLUtils screenLayout] screenWidth], ([[AZLUtils screenLayout]textViewHeight]*4) + 3);
    terms.frame = CGRectMake(30, CGRectGetMaxY(inputView.frame), [[AZLUtils screenLayout] screenWidth] - 60, 30);
    firstName.frame = CGRectMake(0, 0, [[AZLUtils screenLayout] screenWidth], [[AZLUtils screenLayout]textViewHeight]);
    lastName.frame = CGRectMake(0, [[AZLUtils screenLayout]textViewHeight] + 1, [[AZLUtils screenLayout] screenWidth], [[AZLUtils screenLayout]textViewHeight]);
    email.frame = CGRectMake(0, 2*([[AZLUtils screenLayout]textViewHeight] + 1), [[AZLUtils screenLayout] screenWidth], [[AZLUtils screenLayout]textViewHeight]);
    password.frame = CGRectMake(0, 3*([[AZLUtils screenLayout]textViewHeight] + 1), [[AZLUtils screenLayout] screenWidth], [[AZLUtils screenLayout]textViewHeight]);
    footer = [[AZLBottomNavigationBar alloc]initWithButtons:iconBack middle:iconForward right:nil color:[UIColor bluePalette]];
    [footer.buttonLeft addTarget:self
                          action:@selector(returnToLogin:)
                forControlEvents:(UIControlEvents)UIControlEventTouchUpInside];
    [footer.buttonMiddle addTarget:self
                            action:@selector(enterCredentials:)
                  forControlEvents:(UIControlEvents)UIControlEventTouchUpInside];
    
    [super FbLoginInitWithYPos:150]; // Facebook initialize
    
    // Set parameters
    self.view.backgroundColor = [UIColor bluePalette];
    email.keyboardType = UIKeyboardTypeEmailAddress;
    password.secureTextEntry = YES;
    inputView.backgroundColor = [UIColor grayPalette];
    [terms setFont:[UIFont fontWithName:@"Avenir-Light" size:FONT_S]];
    terms.textColor = [UIColor beigePalette];
    
    [self addViews]; // Load in views
}

- (void)addViews
{
    [inputView addSubview:firstName];
    [inputView addSubview:lastName];
    [inputView addSubview:email];
    [inputView addSubview:password];
    [self.view addSubview:fbLoginView];
    [self.view addSubview:footer];
    [self.view addSubview:inputView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)enterCredentials:(id)selector {
    NSLog(@"Firstname: %@, Lastname: %@, Email: %@, Password %@", firstName.text, lastName.text, email.text, password.text);
    // TODO post new user to server
}

- (void)returnToLogin:(id)selector {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
