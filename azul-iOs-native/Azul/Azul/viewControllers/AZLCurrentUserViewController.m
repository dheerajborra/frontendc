//
//  AZLLoginViewController.m
//  Azul
//
//  Created by Tim on 12/19/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import "AZLCurrentUserViewController.h"
#import "AZLNewUserViewController.h"

@interface AZLCurrentUserViewController ()

@end

@implementation AZLCurrentUserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeView];
}

/** Intializes view layout
 */
- (void)initializeView
{
    // Initialize parameter frames
    inputView.frame = CGRectMake(0, 335, [[AZLUtils screenLayout] screenWidth], ([[AZLUtils screenLayout]textViewHeight]*2) + 1);
    email.frame = CGRectMake(0, 0, [[AZLUtils screenLayout] screenWidth], [[AZLUtils screenLayout]textViewHeight]);
    password.frame = CGRectMake(0, [[AZLUtils screenLayout]textViewHeight] + 1, [[AZLUtils screenLayout] screenWidth], [[AZLUtils screenLayout]textViewHeight]);
    newUser.frame = CGRectMake(30, 440, 100, 60);
    footer = [[AZLBottomNavigationBar alloc]initWithButtons:nil middle:iconForward right:nil color:[UIColor bluePalette]];
    
    [super FbLoginInitWithYPos:150]; // Facebook initialize
    
    // Set parameters (custom)
    self.view.backgroundColor = [UIColor bluePalette];
    [newUser setTitle:@"New User" forState:UIControlStateNormal];
    [newUser setTitleColor:[UIColor beigePalette] forState:UIControlStateNormal];
    newUser.contentHorizontalAlignment= UIControlContentHorizontalAlignmentLeft;
    [newUser.titleLabel setFont:[UIFont fontWithName:@"Avenir-Light" size:FONT_L]];
    inputView.backgroundColor = [UIColor grayPalette];
    
    // Assign delegates
    email.delegate = self;
    password.delegate = self;
    
    // Add listeners
    [footer.buttonMiddle addTarget:self
                            action:@selector(enterCredentials:)
                  forControlEvents:(UIControlEvents)UIControlEventTouchUpInside];
    
    // Load in views
    [super addViews];
    [self.view addSubview:footer];
}

- (void)enterCredentials:(id)sender {
    NSLog(@"Username: %@, Password: %@", email.text, password.text);
    NSMutableString *greeting = [NSMutableString stringWithString:@"Hello "];
    [greeting appendString:email.text];
    // TODO post username and password to server
    if([password.text isEqualToString:@"a"]){
        // Correct
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Success!" message:greeting delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
    }else {
        // Incorrect
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login Fail" message:@"The email/password is incorrect" delegate:self cancelButtonTitle:@"Dismiss" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
