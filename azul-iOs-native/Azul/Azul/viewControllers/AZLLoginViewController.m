//
//  AZLLoginViewController.m
//  Azul
//
//  Created by Tim on 12/31/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import "AZLLoginViewController.h"
#import "UIViewController+Animations.h"
#import "AZLBottomNavigationBar.h"

@interface AZLLoginViewController ()

@end

@implementation AZLLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

/** Overrides super thats called when text field is begin editting
 * @param textField - current text field
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up: YES];
}

/** Overrides super thats called when text field is done editting
 * @param textField - current text field
 */
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up: NO];
}

/** Overrides super thats called when return button pressed on keyboard
 * @param textField - current text field
 */
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

/** Initializes login with facebook button and config options
 * @param yPos - y-coordinate of facebook login button
 *
*/
- (void)FbLoginInitWithYPos: (int)yPos
{
    int width = [[AZLUtils screenLayout]screenWidth] - 60;
    fbLoginView = [[FBLoginView alloc] init];
    fbLoginView.frame = CGRectMake([AZLLayoutConfig centerHorizontally:self.view widthOfObject:width], yPos, width, 50);
    fbLoginView.readPermissions = @[@"basic_info", @"email"];
}

/** Add common sub views views to current view
 */
- (void)addViews
{
    [self.view addSubview:fbLoginView];
    [self.view addSubview:footer];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
