//
//  AZLRegisterViewController.h
//  Azul
//
//  Created by Tim on 12/19/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AZLLoginViewController.h"

@interface AZLNewUserViewController : AZLLoginViewController{
    IBOutlet AZLLoginTextField *firstName;
    IBOutlet AZLLoginTextField *lastName;
    IBOutlet AZLLoginTextField *email;
    IBOutlet AZLLoginTextField *password;
    IBOutlet UILabel *terms;
}

- (void)enterCredentials:(id)selector;
- (void)returnToLogin:(id)selector;

@end
