//
//  AZLLoginViewController.h
//  Azul
//
//  Created by Tim on 12/19/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AZLLoginViewController.h"


@interface AZLCurrentUserViewController : AZLLoginViewController{
    IBOutlet AZLLoginTextField *email;
    IBOutlet AZLLoginTextField *password;
    IBOutlet UIButton *newUser;
}

- (void)enterCredentials:(id)sender;

@end
