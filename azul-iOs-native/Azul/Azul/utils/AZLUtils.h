//
//  AZLUtils.h
//  Azul
//
//  Created by Tim on 12/22/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AZLLayoutConfig.h"
#import "AZLConstants.h"

@interface AZLUtils : NSObject

+ (AZLLayoutConfig *)screenLayout;
+ (AZLConstants *)constants;

@end
