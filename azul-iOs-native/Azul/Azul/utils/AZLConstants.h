//
//  AZLConstants.h
//  Azul
//
//  Created by Tim on 12/21/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AZLConstants : NSObject{
    NSString *server;
    NSString *fbAppId;
}

@property NSString *server, *fbAppId;

@end
