//
//  AZLLayoutConfig.h
//  Azul
//
//  Created by Tim on 12/22/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AZLLayoutConfig : NSObject{
    float screenHeight;
    float screenWidth;
    int keyboardHeight;
    int textViewHeight;
}

@property float screenHeight, screenWidth;
@property int keyboardHeight, textViewHeight;

+ (float)centerHorizontally:(UIView *)view widthOfObject:(int)width;

@end
