//
//  NSMutableString+StringUtils.h
//  Azul
//
//  Created by Tim on 12/22/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableString (StringUtils)

+ (NSString *)iconHighlightUrl:(NSString *)url;

@end
