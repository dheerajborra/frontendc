//
//  AZLLayoutConfig.m
//  Azul
//
//  Created by Tim on 12/22/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import "AZLLayoutConfig.h"

@implementation AZLLayoutConfig

@synthesize screenHeight, screenWidth;
@synthesize keyboardHeight, textViewHeight;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [self setScreenParams];
        [self setLayoutParams];
    }
    
    return self;
}

- (void)setScreenParams
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.width;
    screenHeight = screenRect.size.height;
    keyboardHeight = 214;
}

- (void)setLayoutParams
{
    textViewHeight = 55;
}


+ (float)centerHorizontally:(UIView *)view widthOfObject:(int)width
{
    return ((view.frame.size.width - width)/2);
}

@end
