//
//  AZLUtils.m
//  Azul
//
//  Created by Tim on 12/22/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import "AZLUtils.h"

@implementation AZLUtils

+ (AZLLayoutConfig *)screenLayout
{
    static AZLLayoutConfig *screenLayout = nil;
    @synchronized(self)
    {
        if (screenLayout == nil)
        {
            screenLayout = [[AZLLayoutConfig alloc] init];
        }
    }
    return screenLayout;
}

+ (AZLConstants *)constants
{
    static AZLConstants *constants = nil;
    @synchronized(self)
    {
        if (constants == nil)
        {
            constants = [[AZLConstants alloc] init];
        }
    }
    return constants;
}

@end
