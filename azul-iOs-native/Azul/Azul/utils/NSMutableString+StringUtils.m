//
//  NSMutableString+StringUtils.m
//  Azul
//
//  Created by Tim on 12/22/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import "NSMutableString+StringUtils.h"

@implementation NSMutableString (StringUtils)


/** Creates icon highlight url given normal icon url (ex. "forward.png" --> "forward_w.png")
 * @param url - string converted to highlighted url
 * @return highlighted url
 */
+ (NSString *)iconHighlightUrl:(NSString *)url
{
    NSMutableString *highlightedURL = [NSMutableString stringWithCapacity:30];
    [highlightedURL appendString:url];
    NSRange png = [highlightedURL rangeOfString:@".png"];
    [highlightedURL insertString:@"_w" atIndex:png.location];
    NSLog(@"%@",highlightedURL);
    return highlightedURL;
}

@end
