//
//  AZLConstants.m
//  Azul
//
//  Created by Tim on 12/21/13.
//  Copyright (c) 2013 Azul. All rights reserved.
//

#import "AZLConstants.h"

@implementation AZLConstants

@synthesize server, fbAppId;
    
- (id)init
{
    self = [super init];
    if (self)
    {
        [self setParams];
    }
    return self;
}

/** Sets class paramters
 */
- (void)setParams
{
    server = @"http://128.83.196.233:2048";
    fbAppId = @"523831917702051";
}

@end

