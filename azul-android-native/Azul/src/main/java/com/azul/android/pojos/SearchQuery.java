package com.azul.android.pojos;

import org.json.JSONException;
import org.json.JSONObject;

import static android.util.Log.e;

/**
 * The user's query.
 */
public class SearchQuery {

    public String terms;
    public String distance;
    public String cost;
    public String time;

    public SearchQuery(String terms, String cost, String distance, String time) {
        this.terms = terms;
        this.distance = distance;
        this.cost = cost;
        this.time = time;
    }

    public JSONObject toJSON() {
        try {
            return new JSONObject(mockSearchRequest);
        } catch (JSONException e) {
            e(Constants.LOG, "SearchQuery: Exception");
            return null;
        }
    }

    private static final String mockSearchRequest = "{\n" +
            "  \"type\" : \"deal\",\t// search\n" +
            "  \"data\" : {\n" +
            "    \"date_time\": \"2013-10-27T19:00:00\",\n" +
            "    \"distance\": 2,\n" +
            "    \"location\": {\n" +
            "      \"lat\": 30.2794888408268,\n" +
            "      \"lon\": -97.7425112453448\n" +
            "    },\n" +
            "    \"price\": 5,\n" +
            "    \"search_query\": \"Italian\",\n" +
            "    \"user_id\": -1\t\n" +
            "  }\n" +
            "}";

    public String toString() {
        return String.format("%s; %s; %s; %s", terms, distance, cost, time);
    }
}
