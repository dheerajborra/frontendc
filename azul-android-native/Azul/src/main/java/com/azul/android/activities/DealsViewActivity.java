package com.azul.android.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.azul.android.R;
import com.azul.android.adapters.DealArrayAdapter;
import com.azul.android.pojos.Constants;
import com.azul.android.pojos.Deal;
import com.azul.android.pojos.DealThreadManager;
import com.azul.android.pojos.Deals;

import static android.util.Log.*;
import static com.azul.android.pojos.Constants.*;
import static com.azul.android.pojos.DealThreadManager.*;

/**
 * DealsViewActivity handles the View which lists all of the Deals.
 */
public class DealsViewActivity extends Activity {

    ListView dealsList;
    TextView timerTextView;

    // 60 minutes in ms
    long countDown = 2700000;
    long startTime = System.currentTimeMillis();
    Handler timerHandler = new Handler();

    Runnable timerRunnable = new Runnable(){

        @Override
        public void run(){
            if(hasUpdates){
                for(Deal deal : DealThreadManager.getAndClearUpdates()){
                    d(LOG, "DealsViewActivity: updating deals");
                    View rowView = dealsList.getChildAt(deal.position);
                    TextView dealDescriptionView = (TextView) rowView.findViewById(R.id.deal_description);
                    dealDescriptionView.setText(deal.priceReg + " for " + deal.priceNow);
                }
            }
            long millis = countDown - (System.currentTimeMillis() - startTime);
            int seconds = (int) millis / 1000 % 60;
            int minutes = (int) millis / 1000 / 60;
            timerTextView.setText(String.format("%d:%02d", minutes, seconds));
            timerHandler.postDelayed(this, 500);
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_deals);
        timerTextView = (TextView)findViewById(R.id.tv_timer);
        timerHandler.postDelayed(timerRunnable, 0);

        dealsList = (ListView) findViewById(R.id.lv_feed_deals);
        final DealArrayAdapter adapter = new DealArrayAdapter(this, Deals.toArray());
        dealsList.setAdapter(adapter);
        final DealsViewActivity dealsViewActivity = this;
        dealsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int position = adapterView.getPositionForView(view);
                Intent intent = new Intent(dealsViewActivity, DealDetailsActivity.class);
                intent.putExtra("position", position);
                startActivityForResult(intent, 1);
            }
        });
    }

    public void goHome(View view){
        Intent i = new Intent(getApplicationContext(), HomeScreenActivity.class);
        startActivity(i);
    }

}