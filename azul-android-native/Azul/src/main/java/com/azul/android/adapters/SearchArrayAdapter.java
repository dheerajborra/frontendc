package com.azul.android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.azul.android.R;
import com.azul.android.pojos.SearchQuery;

import java.util.ArrayList;

/**
 * Created by Brian on 1/7/14.
 */
public class SearchArrayAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<SearchQuery> searchQueries;

    public SearchArrayAdapter(Context mContext, ArrayList<SearchQuery> searchQueries) {
        this.searchQueries = searchQueries;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        return searchQueries.size();
    }

    @Override
    public Object getItem(int position) {
        return searchQueries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView != null)
            return convertView;

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.search_view_home, parent, false);
        SearchQuery searchQuery = (SearchQuery) getItem(position);
        TextView[] fields = {
                (TextView) view.findViewById(R.id.homeSearchTerms),
                (TextView) view.findViewById(R.id.homeSearchPrice),
                (TextView) view.findViewById(R.id.homeExpiryTime),
                (TextView) view.findViewById(R.id.homeLocationText)
        };

        fields[0].setText(searchQuery.terms);
        fields[1].setText(searchQuery.cost);
        fields[2].setText("Expires: " + searchQuery.time);
        fields[3].setText("Location: " + searchQuery.distance);

        return view;
    }
}
