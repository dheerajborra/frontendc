package com.azul.android.activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.azul.android.R;
import com.azul.android.groups.SliderGroup;
import com.azul.android.groups.TimeGroup;
import com.azul.android.pojos.DealSearch;
import com.azul.android.pojos.SearchQuery;

import java.util.Calendar;
import java.util.Locale;

public class SearchActivity extends Activity implements DatePickerDialog.OnDateSetListener {

    SliderGroup distanceSlider, costSlider;
    TimeGroup timeSlider;
    EditText searchBar;
    Spinner spinner;

    String[] locs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_search);

        Calendar c = Calendar.getInstance();
        String date = String.format("%s, %s %d, %d",
                c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US),
                c.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US),
                c.get(Calendar.DAY_OF_MONTH),
                c.get(Calendar.YEAR)
        );

        timeSlider = new TimeGroup(
                (SeekBar) findViewById(R.id.timeBar),
                (TextView) findViewById(R.id.timeBox),
                date, "%-10s", c.get(Calendar.HOUR_OF_DAY)
        );
        distanceSlider = new SliderGroup(
                (SeekBar) findViewById(R.id.locationBar),
                (TextView) findViewById(R.id.radiusBox),
                "Within %,d miles of: ", 5, 20, 1);

        costSlider = new SliderGroup(
                (SeekBar) findViewById(R.id.valueBar),
                (TextView) findViewById(R.id.valueBox),
                "$%,d value", 5, 100, 5);

        searchBar = (EditText) findViewById(R.id.searchBar);

        spinner = (Spinner) findViewById(R.id.locationBox);
        locs = getResources().getStringArray(R.array.Locations);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, locs);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void changeDate(View view) {
        int[] date = getDate();

        DatePickerDialog picker = new DatePickerDialog(this, this, date[2], date[0], date[1]);
        picker.show();
    }

    public void searchAzul(View view) {
        //TODO call search function
        String terms = searchBar.getText().toString();
        String distance = distanceSlider.getStatus() + locs[(int) spinner.getSelectedItemId()];
        String cost = costSlider.getStatus();
        String time = timeSlider.getStatus().replaceAll("\\s+", " ");
        SearchQuery searchQuery = new SearchQuery(terms, distance, cost, time);
        DealSearch.makeSearchRequest(searchQuery);

        Toast.makeText(this, "Search button: " + searchQuery,
                Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, DealsViewActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        //Check if date is prior to today, if so reject
        Calendar setDate = Calendar.getInstance();
        setDate.set(year, monthOfYear, dayOfMonth);
        if (setDate.before(Calendar.getInstance())) {
            Toast.makeText(this, "Sorry, you have chosen a date which is prior to today. Please pick again.", Toast.LENGTH_LONG).show();
            return;
        }
        String date = String.format("%s, %s %d, %d",
                setDate.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US),
                setDate.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US),
                dayOfMonth,
                year
        );
        timeSlider.setDate(date);

    }

    /**
     * Returns an int array containing the date.
     *
     * @return An int array containing [Month, Day of Month, Year]; Month is 0-indexed.
     */
    public int[] getDate() {
        Calendar c = Calendar.getInstance();
        return new int[]{c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), c.get(Calendar.YEAR)};
    }
}