package com.azul.android.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.azul.android.R;
import com.azul.android.adapters.DealArrayAdapter;
import com.azul.android.pojos.Deal;

/**
 * This Activity handles the details of a specific deal.
 */
public class DealDetailsActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_deal_details);
        Bundle bundle = getIntent().getExtras();
        int position = (Integer) bundle.get("position");
        Deal deal = DealArrayAdapter.deals[position];

        TextView restaurantName = (TextView) findViewById(R.id.restaurant_name);
        restaurantName.setText(deal.merchant);

    }
}