package com.azul.android.pojos;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.util.Log.d;
import static com.azul.android.pojos.Constants.LOG;
import static com.azul.android.pojos.DealThreadManager.pendingUpdates;

/**
 * Deals is used to hold Deal objects at a public static visibility.
 */
public class Deals {

    public static Map<String, Deal> mostRecentResults = new HashMap<String, Deal>();

    public static void setMostRecent(JSONObject response) {
        try {
            d(LOG, "Deals: response -> " + response.toString());
            JSONArray resultsArr = response.getJSONArray("results");
            for (int i = 0; i < resultsArr.length(); i++) {
                Deal deal = new Deal(resultsArr.getJSONObject(i));
                Log.d(LOG, "Deals: putting " + deal.dealID + ": " + deal.toString());
                mostRecentResults.put(deal.dealID, deal);
            }
            pendingUpdates.clear();
        } catch (JSONException e) {
            d(LOG, "Deals: Exception");
        }
    }

    public static Deal[] toArray() {
        return mostRecentResults.values().toArray(new Deal[0]);
    }

    public static Deal getDealByID(String dealID) {
        d(LOG, "Deals: " + dealID + " not found, keyset is " + mostRecentResults.keySet().toString());
        return mostRecentResults.get(dealID);

    }
}
