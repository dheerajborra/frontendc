package com.azul.android.pojos;

import android.os.Handler;

import org.json.JSONException;
import org.json.JSONObject;

import static android.util.Log.d;
import static com.azul.android.pojos.Constants.LOG;

/**
 * Created by jeffreymeyerson on 12/22/13.
 */
public class Server {

    public enum RequestType {POST, GET}

    // TODO this is mocked bidding
    private static Handler mockBiddingHandler = new Handler();
    private static Runnable mockBiddingRunnable = new Runnable() {
        @Override
        public void run() {
            receiveResponse(getRandomMockUpdate());
            mockBiddingHandler.postDelayed(this, 3000);
        }
    };

    public static void issueRequest(RequestType requestType, String URL, JSONObject request) {
        //TODO request stuff
        try {
            receiveResponse(new JSONObject(mockSearchResponse));
            d(LOG, "Server: response received");
        } catch (JSONException e) {
            d(LOG, "Server: " + e.toString());
        }
    }

    // TODO: Put this on a listener thread

    /**
     * When a response is received on the listener, this method hands it off to the proper object
     */
    public static void receiveResponse(JSONObject resp) {
        if (resp.has("results")) {
            d(LOG, "Server: recvResp -> " + resp.toString());
            Deals.setMostRecent(resp);
        }
        if (resp.has("update")) {
            try {
                DealThreadManager.updateDeal(resp.getJSONObject("update"));
            } catch (JSONException e) {
                d(LOG, e.toString());
            }
        }
    }

    private static final String mockSearchResponse =
            "{\n" +
                    "\"query_id\":”M5lWsrnISHO5we39qW6j1A”,\n" +
                    "\"results\" :[{\n" +
                    "\t\"favorite\": true,\n" +
                    "\t\"deal_id\": \"vKKfrrcrRCmWv7-k0DTDtB\",\n" +
                    "\t\"address\": \"123 Fake St, Austin, TX 78759\",\n" +
                    "\t\"category\": \"American\",\n" +
                    "\t\"description\": \"Best American food\",\n" +
                    "\t\"distance\": 1.0124,\n" +
                    "\t\"fineprint\": \"(c) 2013 Azul Mobile Inc.\",\n" +
                    "\t\"img\": \"http://cdn.draxe.com/wp-content/uploads/2012/09/butter.jpg\",\n" +
                    "\t\"location\": {\n" +
                    "\t\t\"lat\": 30.28,\n" +
                    "\t\t\"lon\": -97.720\n" +
                    "\t},\n" +
                    "\t\t\"merchant\": \"Butter Emporium\",\n" +
                    "\t\"price_now\": 22.50,\n" +
                    "\t\"price_prev\": 27.50,\n" +
                    "\t\"price_reg\": 44.99,\n" +
                    "\t\"rating\": 4.5,\n" +
                    "\t\"inventory\": 10,\n" +
                    "\"merchant_id\": 126, \n" +
                    "\"max_price\": 14.50, \n" +
                    "\"min_price\": 12.50, \n" +
                    "\"bid_window\": \"2013-11-28 17:00:00\"\n" +
                    "},\n" +
                    "{" +
                    "\t\"favorite\": false,\n" +
                    "\t\"deal_id\": \"vKKfrrcrRCmWv7-k0DTDtA\",\n" +
                    "\t\"address\": \"10000 Research Blvd Ste, Austin, TX 78759\",\n" +
                    "\t\"category\": \"Italian\",\n" +
                    "\t\"description\": \"describe deal here\",\n" +
                    "\t\"distance\": 1.0124,\n" +
                    "\t\"fineprint\": \"(c) 2013 Azul Mobile Inc.\",\n" +
                    "\t\"img\": \"http://www.bluestatebbq.files.wordpress.com/2011/04/1273787024058.png\",\n" +
                    "\t\"location\": {\n" +
                    "\t\t\"lat\": 30.28,\n" +
                    "\t\t\"lon\": -97.720\n" +
                    "\t},\n" +
                    "\t\t\"merchant\": \"Butch's Deli\",\n" +
                    "\t\"price_now\": 19.50,\n" +
                    "\t\"price_prev\": 19.50,\n" +
                    "\t\"price_reg\": 29.99,\n" +
                    "\t\"rating\": 3.5,\n" +
                    "\t\"inventory\": 15,\n" +
                    "\"merchant_id\": 146, \n" +
                    "\"max_price\": 19.50, \n" +
                    "\"min_price\": 10.50, \n" +
                    "\"bid_window\": \"2013-10-27 19:00:00\"\n" +
                    "}],\n" +
                    "\"others\" : [{\n" +
                    "   \"merchant_id\" : \"vKKfrrcrRCmWv7-k0DTDtA\",\n" +
                    "   \"location\":{\n" +
                    "\t         \"lat\": 30.250252219711278,\n" +
                    "\t         \"lon\": -97.73623287770891\n" +
                    "\t\t},\n" +
                    "   \"rating\":       \"3.5\",\n" +
                    "   \"name\":         \"Dick’s Kitchen\",\n" +
                    "   \"categories\":   \"Italian\",\n" +
                    "   \"price_range\":  \"$$\"\n" +
                    "}" +
                    "]\n" +
                    "}";

    public static void runMockBidding() {
        mockBiddingHandler.postDelayed(mockBiddingRunnable, 0);
    }

    public static JSONObject getRandomMockUpdate() {
        try {
            JSONObject mockUpdate1 = new JSONObject("{update : {deal_id : \"vKKfrrcrRCmWv7-k0DTDtB\", price_change : decrement}}");
            d(LOG, "mockUpdate1 -> " + mockUpdate1.toString());
            return mockUpdate1;
        } catch (JSONException e) {
            d(LOG, "Parsing JSON incorrectly in random mock update");
        }
        return null;
    }

}
