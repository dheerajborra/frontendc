package com.azul.android.pojos;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jeffreymeyerson on 12/21/13.
 */
public class Deal {

    // This deal's position in the ListView, total hack
    public int position;

    public boolean favorite;
    public String dealID;
    public String address;
    public String category;
    public String description;
    public double distance;
    public String fineprint;
    public String img;
    public Map location;
    public String merchant;
    public double priceNow;
    public double pricePrev;
    public double priceReg;
    public double rating;
    public int inventory;

    /**
     * @param obj should be a "unified deal object"
     */
    public Deal(JSONObject obj) {
        try {
            favorite = obj.getBoolean("favorite");
            dealID = obj.getString("deal_id");
            address = obj.getString("address");
            category = obj.getString("category");
            description = obj.getString("description");
            distance = obj.getDouble("distance");
            fineprint = obj.getString("fineprint");
            img = obj.getString("img");
            JSONObject locObj = obj.getJSONObject("location");
            location = new HashMap<Object, Object>();
            location.put("lat", locObj.get("lat"));
            location.put("lon", locObj.get("lon"));
            merchant = obj.getString("merchant");
            priceNow = obj.getDouble("price_now");
            pricePrev = obj.getDouble("price_prev");
            priceReg = obj.getDouble("price_reg");
            rating = obj.getDouble("rating");
            inventory = obj.getInt("inventory");
        } catch (JSONException e) {
            Log.d(Constants.LOG, e.toString());
        }
    }

    public String toString() {
        return "Deal: {Merchant = " + merchant + "}";
    }


}
