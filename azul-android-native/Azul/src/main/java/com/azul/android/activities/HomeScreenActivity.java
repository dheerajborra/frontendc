package com.azul.android.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.azul.android.R;
import com.azul.android.adapters.SearchArrayAdapter;
import com.azul.android.pojos.Constants;
import com.azul.android.pojos.SearchQuery;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class HomeScreenActivity extends Activity {

    ListView searchList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        searchList = (ListView)findViewById(R.id.searchList);
        SearchArrayAdapter adapter = new SearchArrayAdapter(getApplicationContext(),
                readSearches());
        searchList.setAdapter(adapter);
        searchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //TODO Put something here. Go to DealsViewScreen maybe??
                //TODO provide some way to remove deals
            }
        });

        if (!new File(getFilesDir(), "AccountDetails.txt").exists()) {
            Intent register = new Intent(this, RegistrationActivity.class);
            startActivity(register);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void startSearchActivity(MenuItem item) {
        Intent i = new Intent(getApplicationContext(), SearchActivity.class);
        startActivity(i);
    }

    //TODO we should be constructing the home screen searches off of previous SearchResults
    public ArrayList<SearchQuery> readSearches() {
        ArrayList<SearchQuery> searches = new ArrayList<SearchQuery>();
        try {
            File f = new File(getFilesDir(), "searches.txt");
            if (!f.exists())
                return searches;

            Scanner scanner = new Scanner(f);
            while (scanner.hasNextLine()) {
                String terms = scanner.nextLine();
                String cost = scanner.nextLine();
                String distance = scanner.nextLine();
                String time = scanner.nextLine();
                searches.add(new SearchQuery(terms, cost, distance, time));
                scanner.nextLine();
            }

        } catch (Exception e) {
            Log.d(Constants.LOG, e.toString());
        }
        return searches;

    }

}
