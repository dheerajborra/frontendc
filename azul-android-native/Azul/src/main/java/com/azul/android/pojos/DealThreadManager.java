package com.azul.android.pojos;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.util.Log.d;
import static com.azul.android.pojos.Constants.LOG;

/**
 * This class handles updating Deal objects asynchronously.
 */
public class DealThreadManager {

    // DealDetailsActivity will only update if this isn't empty; trying to minimize UIThread use
    public static List<Deal> pendingUpdates = new ArrayList<Deal>();
    public static boolean hasUpdates = false;

    public static void updateDeal(JSONObject resp) {
        try {
            d(LOG, "DealThreadManager: recv-> " + resp.toString());
            String dealID = resp.getString("deal_id");
            Deal deal = Deals.getDealByID(dealID);
            if (deal == null) {
                d(LOG, "DealThreadManager: " + dealID + " not present in deals.");
                return;
            }
            deal.pricePrev = deal.priceNow;
            if (resp.getString("price_change").equals("decrement")) {
                deal.priceNow = deal.priceNow - 1;
            } else {
                deal.priceNow = deal.pricePrev + 1;
            }
            d(LOG, "DealThreadManager: Update received");
            pendingUpdates.add(deal);
            hasUpdates = true;
        } catch (JSONException e) {
            d(LOG, "Exception in DealThreadManager: " + e.toString());
        }
    }

    public static Deal[] getAndClearUpdates() {
        Deal[] updates = pendingUpdates.toArray(new Deal[0]);
        pendingUpdates.clear();
        hasUpdates = false;
        return updates;
    }

}
