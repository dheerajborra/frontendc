package com.azul.android.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.azul.android.R;
import com.azul.android.pojos.Constants;
import com.azul.android.pojos.Deal;
import com.azul.android.pojos.Deals;

import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;

import static android.util.Log.*;

/**
 * Created by jeffreymeyerson on 12/21/13.
 */
public class DealArrayAdapter extends ArrayAdapter<Deal> {

    private final Context context;
    public static Deal[] deals;

    public DealArrayAdapter(Context context, Deal[] deals) {
        super(context, R.layout.deal_layout, deals);
        d(Constants.LOG, "Deals: " + Arrays.toString(Deals.toArray()));
        this.deals = deals;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Deal deal = deals[position];
        deal.position = position;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.deal_layout, parent, false);
        TextView restaurantNameView = (TextView) rowView.findViewById(R.id.restaurant_name);
        TextView foodTypeView = (TextView) rowView.findViewById(R.id.food_type);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        TextView dealDescriptionView = (TextView) rowView.findViewById(R.id.deal_description);

        restaurantNameView.setText(deal.merchant);
        foodTypeView.setText(deal.category);
        dealDescriptionView.setText(deal.priceReg + " for " + deal.priceNow);

        // set image
        try {
            URL url = new URL(deal.img);
            InputStream content = (InputStream) url.getContent();
            Drawable d = Drawable.createFromStream(content, "src");
            imageView.setImageDrawable(d);
        } catch (Exception e) {
            d(Constants.LOG, "DealArrayAdapter: Exception");
        }

        return rowView;

    }
}
