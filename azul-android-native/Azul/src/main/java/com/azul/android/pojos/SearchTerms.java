package com.azul.android.pojos;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import static android.util.Log.*;

/**
 * Created by jeffreymeyerson on 12/21/13.
 */
public class SearchTerms {

    public String query;
    public String distance;
    public String cost;
    public String time;

    public SearchTerms(String query, String distance, String cost, String time) {
        this.query = query;
        this.distance = distance;
        this.cost = cost;
        this.time = time;
    }

    public JSONObject toJSON() {
        try {
            return new JSONObject(mockSearchRequest);
        } catch (JSONException e) {
            e(Constants.LOG, "SearchTerms: Exception");
            return null;
        }
    }

    private static final String mockSearchRequest = "{\n" +
            "  \"type\" : \"deal\",\t// search\n" +
            "  \"data\" : {\n" +
            "    \"date_time\": \"2013-10-27T19:00:00\",\n" +
            "    \"distance\": 2,\n" +
            "    \"location\": {\n" +
            "      \"lat\": 30.2794888408268,\n" +
            "      \"lon\": -97.7425112453448\n" +
            "    },\n" +
            "    \"price\": 5,\n" +
            "    \"search_query\": \"Italian\",\n" +
            "    \"user_id\": -1\t\n" +
            "  }\n" +
            "}";

    public String toString() {
        return String.format("%s; %s; %s; %s", query, distance, cost, time);
    }
}
