package com.azul.android.groups;

import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by Brian on 12/13/13.
 */
public class SliderGroup {

    /**
     * Slider bar
     */
    SeekBar bar;
    /**
     * View that displays formatted text
     */
    TextView view;
    /**
     * A format String for the TextView (printf format)
     */
    String formatString;
    /**
     * At what value the slider should start.
     */
    int start,
    /**
     * At what value the slider should end
     */
    end,
    /**
     * The value by which the slider increments.
     */
    increment;

    /**
     * Constructs a new SliderGroup.
     *
     * @param b      The slider used in this group.
     * @param v      The view to display text to.
     * @param format The format string used to format the text.
     * @param first  Where the slider should start.
     * @param last   Where the slider should end.
     * @param incr   What the slider should increment by.
     */
    public SliderGroup(SeekBar b, TextView v, String format, int first, int last, int incr) {
        bar = b;
        view = v;
        start = first;
        end = last;
        increment = incr;
        formatString = format;

        bar.setMax((end - start) / increment);

        view.setText(getStatus());

        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                view.setText(getStatus());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    /**
     * Returns a formatted String for use in the TextView
     *
     * @return A formatted String relating to the slider's current status.
     */
    public String getStatus() {
        return String.format(formatString, (bar.getProgress() * increment) + start);
    }

    /**
     * Sets the slider to a certain value, and updates the TextView accordingly.
     *
     * @param progress Where to set the slider to.
     */
    public void setSlider(int progress) {
        bar.setProgress(progress);
        view.setText(getStatus());
    }

    public String toString() {
        return String.format("SliderGroup, range: %,d-%,d; format: %s", start, end, formatString);
    }
}
