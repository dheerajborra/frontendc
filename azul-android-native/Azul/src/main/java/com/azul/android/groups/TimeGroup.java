package com.azul.android.groups;

import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by Brian on 12/13/13.
 */
public class TimeGroup extends SliderGroup {
    String date, format;

    /**
     * Constructs a new SliderGroup.
     *
     * @param b      The slider used in this group.
     * @param v      The view to display text to.
     * @param format The format string used to format the text.
     * @param start  Where the slider should start.
     */
    public TimeGroup(SeekBar b, TextView v, String date, String format, int start) {
        super(b, v, format + date, start, 23, 1);
        this.date = date;
        this.format = format;
    }

    public String getStatus() {
        int time = (bar.getProgress() + start) % 12;
        if (time % 12 == 0)
            time = 12;

        return (bar.getProgress() + start) >= 12 ? String.format(formatString, time + "PM") :
                String.format(formatString, time + "AM");
    }

    /**
     * Sets a new date for the TextBox.
     *
     * @param s The date to be set.
     */
    public void setDate(String s) {
        date = s;
        formatString = format + date;
        view.setText(getStatus());
    }
}
