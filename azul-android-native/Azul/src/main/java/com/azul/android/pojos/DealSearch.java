package com.azul.android.pojos;

import org.json.JSONObject;

import static com.azul.android.pojos.Server.RequestType;
import static com.azul.android.pojos.Server.issueRequest;
import static com.azul.android.pojos.Server.runMockBidding;

/**
 * This class sits between the app and the Elastic Search server functionality.
 */
public class DealSearch {

    public static void makeSearchRequest(SearchQuery searchQuery) {
        // call ElasticSearch
        JSONObject request = searchQuery.toJSON();
        issueRequest(RequestType.POST, "/search", request);
        runMockBidding();

    }

}
