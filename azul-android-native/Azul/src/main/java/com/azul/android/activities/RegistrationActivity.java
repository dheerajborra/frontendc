package com.azul.android.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.azul.android.R;
import com.azul.android.pojos.Constants;
import com.facebook.LoggingBehavior;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Settings;
import com.facebook.model.GraphUser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import static com.azul.android.activities.RegistrationActivity.LoginState.*;

/**
 * Registration Page //TODO, refactor and change to LoginActivity
 */
public class RegistrationActivity extends Activity {

    //TODO This could handle different registration cases, ie Facebook
    public enum LoginState {NOT_LOGGED_IN,
                            NOT_LOGGED_IN_BUT_CACHED_CREDENTIALS,
                            LOGGED_IN,
                            FACEBOOK_REGISTERED}

    EditText nameBox;
    EditText emailBox;
    EditText passwordBox;
    EditText confirmPasswordBox;

    Button register;
    Button login;

    /**
     * Boolean governing which elements are visible
     * depending on which mode the page is in (existing user or new user).
     */
    LoginState loginState = NOT_LOGGED_IN;
    boolean loginSelect = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        nameBox = (EditText) findViewById(R.id.nameBox);
        emailBox = (EditText) findViewById(R.id.emailBox);
        passwordBox = (EditText) findViewById(R.id.passwordBox);
        confirmPasswordBox = (EditText) findViewById(R.id.confirmPasswordBox);

        register = (Button) findViewById(R.id.registerButton);
        login = (Button) findViewById(R.id.loginButton);

        Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        Settings.addLoggingBehavior(LoggingBehavior.REQUESTS);

        // Test for Facebook login
        Request request = Request.newGraphPathRequest(null, "/4", new Request.Callback() {
            @Override
            public void onCompleted(Response response) {
                if(response.getError() != null) {
                    Log.i("MainActivity", String.format("Error making request: %s", response.getError()));
                } else {
                    GraphUser user = response.getGraphObjectAs(GraphUser.class);
                    Log.i("MainActivity", String.format("Name: %s", user.getName()));
                }
            }
        });
        request.executeAsync();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Method called when the Existing User button is clicked. The confirm password and name boxes disappear
     * and the Existing User button becomes the Login button.
     *
     * @param view
     */
    public void startLoginActivity(View view) {
        if (!loginSelect) {
            emailBox.setVisibility(View.VISIBLE);
            passwordBox.setVisibility(View.VISIBLE);
            login.setText("Log In");

            loginSelect = true;
            return;
        }

        if (loginState == NOT_LOGGED_IN) {
            nameBox.setVisibility(View.GONE);
            confirmPasswordBox.setVisibility(View.GONE);
            login.setText("Log In");
            register.setText("Create a New Account");
            loginState = LOGGED_IN;
            return;
        }

        Toast.makeText(getApplicationContext(), "Post to Server for Login", Toast.LENGTH_LONG).show();
        try {
            PrintStream p = new PrintStream(new File(getFilesDir(), "AccountDetails.txt"));
            p.println("Name:Dummy McServer");
            //TODO password in plaintext
            p.println("Password:" + passwordBox.getText());
            //TODO get email from server?!
            p.println("Email:" + emailBox.getText());

            p.close();
            finish();
        } catch (FileNotFoundException e) {
            Toast.makeText(getApplicationContext(), "Login Unsuccessful!", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    /**
     * Method called when the register button is clicked.
     * Checks if the passwords match and if so registers the user.
     *
     * @param view
     */
    public void registerUser(View view) {
        if (!loginSelect) {
            nameBox.setVisibility(View.VISIBLE);
            emailBox.setVisibility(View.VISIBLE);
            passwordBox.setVisibility(View.VISIBLE);
            confirmPasswordBox.setVisibility(View.VISIBLE);
            register.setText("Register");
            login.setText("Log In as Existing User");
            loginSelect = true;
            return;
        }

        if (loginState == NOT_LOGGED_IN_BUT_CACHED_CREDENTIALS) {
            register.setText("Register");
            login.setText("Log In as Existing User");
            nameBox.setVisibility(View.VISIBLE);
            confirmPasswordBox.setVisibility(View.VISIBLE);
            return;
        }

        if(!verifyFields()){
            return;
        }

        try {
            PrintStream p = new PrintStream(new File(getFilesDir(), "AccountDetails.txt"));
            p.println("Name:" + nameBox.getText());
            p.println("Password:" + passwordBox.getText());
            p.println("Email:" + emailBox.getText());
            Toast.makeText(this, "Post to Server for Registration", Toast.LENGTH_LONG).show();
            p.close();
            finish();
        }
        catch (FileNotFoundException e) {
            Toast.makeText(this, "Registration Unsuccessful!", Toast.LENGTH_LONG).show();
            Log.d(Constants.LOG, e.toString());
        }
    }

    /**
     * Checks that necessary fields are filled in.
     *
     * @return
     */
    private boolean verifyFields(){
        for (EditText e : new EditText[]{nameBox, emailBox, passwordBox, confirmPasswordBox}) {
            if (e.getText().toString().matches("")) {
                Toast.makeText(this, "One or more of the fields above is empty. " +
                        "Please enter information and try again!", Toast.LENGTH_LONG).show();
                return false;
            }
        }
        if (!passwordBox.getText().equals(confirmPasswordBox.getText().toString())) {
            Toast.makeText(this, "The passwords don't match. Please try again!", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    //Disables the back button so that the user can't use the app without logging in/creating an account.
    public void onBackPressed() {
    }
}
