var socket = io.connect('http://128.83.196.226:8181', {
	'sync disconnect on unload' : true,
	'force new connection' : true
});

// var socket = {
// 	emit: function(a, b) {},
// 	on: function(a, b) {}
// };

var socket_userdata = null;

function socket_initialize() {
	if (socket_userdata != null) {
		console.log('socketapp: init with user object - ' + JSON.stringify(socket_userdata));
		socket.emit('initialize', socket_userdata);
	}
}

function socket_logout() {
	console.log("socketapp: sending a logout note");
	socket.emit('logout');
}

function socket_sendFav(data) {
	console.log("socketapp: sending favorite - " + data);
	console.log(data);
	socket.emit('favorite', data);
}

function socket_sendSuggest(data) {
	console.log("socketapp: sending suggest - " + data);
	console.log(data);
	socket.emit('suggest', data);
}

function socket_cancelSearch() {
	console.log("socketapp: sending cancel - " + data);
	socket.emit('cancelSearch');
}

function socket_sendTrash(data) {
	console.log("socketapp: sending trash - " + data);
	console.log(data);
	socket.emit('trash', data);
}

function socket_sendBid(data) {
	console.log("socketapp: sending bid - " + data);
	console.log(data);
	socket.emit('bid', data);
}

socket.on('updateBid', function (data) {
	console.log("socketapp: got a bid update - " + data);
	console.log(data);
	// TODO: do something here
});

socket.on('connect', function () {
	console.log("socketapp: connected to server!");
	socket_initialize();
});
