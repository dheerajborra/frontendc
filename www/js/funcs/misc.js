$.fn.stars = function() {
    return $(this).each(function() {
        // Get the value
        var val = $(this).attr('rating');
        val = Math.round(val * 4) / 4; /* To round to nearest quarter */
        // Make sure that the value is in 0 - 5 range, multiply to get width
        var size = Math.max(0, (Math.min(5, val))) * 16;
        // Create stars holder
        var $span = $('<span />').width(size);
        // Replace the numerical value with stars
        $(this).html($span);
    });
}

function make_year_options(num_years, step){
	var html = '<option value="0000" selected disabled>Year</option>';
	var current_year = (new Date()).getFullYear();
    for(var i = 0; Math.abs(i*step) <= num_years; i++){
        html += '<option value="' + (current_year + i*step) + '">' + (current_year + i*step) + '</option>';
    }
    return html;
}

function make_year_card(num_years_ahead){
    var current_year = (new Date()).getFullYear();
    var html = '<option value="'+current_year+'" selected>'+current_year+'</option>';
    for(var i = current_year+1; i <= current_year + num_years_ahead; i++){
        html += '<option value="' + i + '">' + i + '</option>';
    }
    return html;
}

function resetBrowseData(){
    deals = [];
    otherMerchants = [];
    bidwindow = 0;
    sortedDeals = [];
    cash_remove("search");
}

function parseDeals(deals){
    for(var x = 0; x<deals.length; x++){
        deals[x]["priceNow"] = Math.floor(Ember.get(deals[x],'price_now'));
        deals[x]["pricePrev"] = Math.floor(Ember.get(deals[x],'price_prev'));
        deals[x]["priceNowCents"] = leading_zero(Math.round(Ember.get(deals[x],'price_now')*100)%100);
        deals[x]["pricePrevCents"] = leading_zero(Math.round(Ember.get(deals[x],'price_prev')*100)%100);
    }
    return deals;
}

function loginInit(authData, userData) {
    socket_userdata = {id: authData.id, token: authData.token}
    socket_initialize();
    console.log('getting cards for purchase');
    $.get(server + "/users/" + authData.id + "/cards")
        .done(function(data) {
            console.log("got some cards");
            console.log(data.cards);
            Cards = data.cards;
        })
        .fail(function(data) {
            console.log("oops something went wrong -- don't care");
        });
}

function checkLogin(view){
    console.log("CEHCKING LOGIN");
    if(view.get('controller.currentUser') == null){
        view.get('controller').transitionToRoute('login');
    }
}