var azul_cash = window.localStorage;

function cash_add(k, v){
	try{
		azul_cash.setItem(k, v);
	}catch(err){
		console.log('cash add fail');
		return false;
	}
	console.log('cash add success');
	return true;
}

function cash_get(k){
	try{
		var v = azul_cash.getItem(k);
	}catch(err){
		console.log('cash get fail');
		return false;
	}
	if(v == null){
		console.log('cash get fail');
		return false;
	}
	return v;
}

function cash_remove(k){
	try{
		azul_cash.removeItem(k);
	}catch(err){
		console.log('cash remove fail');
		return false;
	}
	return true;
}

function cash_clear(){
	try{
		azul_cash.clear();
	}catch(err){
		console.log('cash clear fail');
		return false;
	}
	return true;
}

// updateCache updates local storage
function updateCache(){
	var data = {
		others: otherMerchants,
		results: deals,
		query_id: searchQuery,
		bidwindow : bidwindow,
	}
	cash_add("search", JSON.stringify(data));
};
