function leading_zero(num){
	return (num < 10 ? '0' : '') + num;
}

function twenty_four_to_twelve(hours){
	var twenty_four_to_twelve_conversion = {
		0: 12,
		1: 1,
		2: 2,
		3: 3,
		4: 4,
		5: 5,
		6: 6,
		7: 7,
		8: 8,
		9: 9,
		10: 10,
		11: 11,
		12: 12,
		13: 1,
		14: 2,
		15: 3,
		16: 4,
		17: 5,
		18: 6,
		19: 7,
		20: 8,
		21: 9,
		22: 10,
		23: 11,
	}
	return twenty_four_to_twelve_conversion[hours];
}

function rfc3339_d(js_date){
	return js_date.getFullYear() + '-'
    + leading_zero(js_date.getMonth() + 1)+ '-'
    + leading_zero(js_date.getDate());
}

function rfc3339_t(js_date){
	return leading_zero(js_date.getHours()) + ':'
    + leading_zero(js_date.getMinutes());
}

function rfc3339_ts(js_date){
	return rfc3339_t(js_date) + ':'
    + leading_zero(js_date.getSeconds());
}

function rfc3339_tsm(js_date){
	return rfc3339_ts(js_date) + '.' + js_date.getMilliseconds();
}

function rfc3339_dt(js_date){
	return rfc3339_d(js_date) + 'T' + rfc3339_ts(js_date);
}

function time_am_pm(js_date, upper){
	return js_date.getHours() < 12 ? 'AM' : 'PM';
}

function twelve_hour_time(js_date){
	return twenty_four_to_twelve(js_date.getHours()) + ':'
    + leading_zero(js_date.getMinutes())
    + '&nbsp;' + time_am_pm(js_date);
}

function round_to_thirty(value){
	return Math.round((value + 15) / 30) * 30;
}
