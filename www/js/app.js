// App.js contains all Ember.js modules for Azul Mobile Beta
//
// Author: Azul Frontend
// Version: 1.0
// Date: 12/8/2013


/***************************************
	App Constant's & Global Objects
***************************************/
var server = "http://128.83.196.226:2048"; // Web server
var fbAppId = "523831917702051"; // Fb App Id
var fbAppScope = "email,user_birthday,user_location"; // Fb user info
var stripeKey = "pk_live_HyUICOnX5ESf36nctPyEhYFl"; // Stripe key

var bidwindow = ""; // Bid window for current search
var countdown = 0; // Global timer for bid window
var searchQuery = 0; // Search query id
var refreshTime = false; // flag that resets timer on "resume" event
var Cards = []; // Array of user's credit cards
var otherMerchants = []; // Other merchant for suggesting
var purchased_deal = {}; // Last current purchase || model for voucher view
var purchases = []; // List of user's purchase history
var deals = []; // List of deals from search query result

/***************************************
	App Configuration
***************************************/
App = Ember.Application.create({
	currentPath: '', // current state 
	previousPath: '', // previous state
	bidDelay: false, // flag for fake bidding (reset on 10 minute timeout)
	// On application creation
	ready: function(){
		resize(); // Resize containers
		// Event listener for when application is removed from background
		document.addEventListener("resume", resumeGo, false);
		// Event listener for new socket bid update
        socket.on('updateBid', function (data) {
			console.log("socketapp: got a bid update - ");
			console.log(data);
			// Update deal
			if(deals.length == 0){
				var cash = cash_get("search");
				if(cash){
					cash = jQuery.parseJSON(cash);
		   	 		deals = cash.results;
				}
			}
			// If updated deals of length, find and animate deal
			if (deals.length != 0){
				var deal = deals.findBy('deal_id', data.deal_id);
				var index = deals.indexOf(deal);
				var isTile = true;
				var pricePrev = Ember.get(deal, 'price_now');
				var remaining = Ember.get(deal, 'inventory');
				var new_inventory = data.inventory;
				console.log(deal);
				// Update button color if inventory different
				if(remaining != new_inventory){
					var deal_space = null;
					var deal_tile = null;
					Ember.set(deal, 'inventory', new_inventory); // Update Deal inventory
					// Find Deal on screen
					$('.deal-space').each(function(){
						if($(this).attr('deal_id') == deal.deal_id){
							deal_space = $(this);
							deal_tile = deal_space.parent('.deal-tile');
							return false;
						}
					});
					var buy_btn = deal_space.find('#buy-btn');
					if(new_inventory > 5){
						buy_btn.addClass('button-green');
					}else if(new_inventory >= 3){
						buy_btn.removeClass('button-green');
						buy_btn.addClass('button-yellow');
					}else if(new_inventory > 0){
						buy_btn.removeClass('button-yellow');
						buy_btn.addClass('button-red');
					}
					else{
						deal_tile.remove();
						deals = deals.slice(0, index).concat(deals.slice(index+1, deals.length));
						isTile = false;
					}
					updateCache();
				}
				// Update price if price change
				if (data.price_now == pricePrev){
					console.log("SAME PRICE NO ANIMATION");
				}
				else if (isTile){
					updateDeal(deal, data);
					// Cache deals again
					updateCache();
					// Change UI based on state
					if(App.get('currentPath') == 'browse'){
						animateBrowse(deal);
					}
				}
			}
		});

		// Create fakebid on a random deal if in browse deals state
		function resumeGo(){
			var currState = App.get('currentPath');
			var bidDelay = App.get('bidDelay');
			refreshTime = true; // Set flag to reset timer
			if ( (currState == 'browse') && (deals.length >0) ){
				if (!bidDelay){
					fakeBid(); // If browse state, continueing deals and delay is over, fake bid
				}
			}
		};

		// fakeBid() method creates fake bid price and animates deal tile
		function fakeBid(){
			console.log("FAKE BID");
			// Declare variables
			var fake_bid = {
				"deal_id": '',
				"price_now": 0,
			}
			var randomIndex = Math.floor(Math.random()*deals.length);
			var randomDeal = deals[randomIndex]; // Get random deal in array
			var deal_price = Ember.get(randomDeal, 'price_now');
			var deal_id = Ember.get(randomDeal, 'deal_id');
			
			// Update fake bid
			fake_bid.deal_id = deal_id;	
			fake_bid.price_now = ( deal_price - (deal_price*0.15) ); // Subtract 15% off of deal

			setTimeout(function(){
				updateDeal(randomDeal, fake_bid); // Update deal
				animateBrowse(randomDeal); // Animate deal
				App.set('bidDelay', true);
				// Wait 10 minutes to reset fakeBid flag
				setTimeout(function(){
					console.log("RESET");
					App.set('bidDelay', false);
				}, 600000);
			}, 200);

		};

		// updateDeal() method updates corresponding deal given data
		function updateDeal(deal, data){
			var pricePrev = Ember.get(deal, 'price_now');
			Ember.set(deal, 'price_prev', data.price_now);
			Ember.set(deal, 'price_prev', pricePrev);
			Ember.set(deal, 'price_now', data.price_now);
			Ember.set(deal, 'time_stamp', (new Date()).getTime());
			Ember.set(deal, "priceNow", Math.floor(Ember.get(deal,'price_now')));
	        Ember.set(deal, "pricePrev", Math.floor(Ember.get(deal,'price_prev')));
	        Ember.set(deal, "priceNowCents", leading_zero(Math.round(Ember.get(deal,'price_now')*100)%100));
	        Ember.set(deal, "pricePrevCents", leading_zero(Math.round(Ember.get(deal,'price_prev')*100)%100));
		};

		// resize() method resizes app parent containers on load
		function resize(){
			// Declare variables
			var style;
			var style_rules = [];
			var screen_height = document.height;
			var nav_height = screen_height*0.08;
			var contain_height = screen_height*0.84;
			var contain_NoHead = screen_height*0.92;
			var timer_height = screen_height*0.05;
			var timer_top = screen_height*0.025;

			// Append to css file
			style_rules.push(".contain-noheader{ height:+"+contain_NoHead+"px !important; } ");
			style_rules.push(".demo-contain{ height:+"+contain_height+"px; } ");
			style_rules.push(".contain{ height:+"+contain_height+"px; } ");
			style_rules.push("#header{ height:+"+nav_height+"px; } ");
			style_rules.push(".footer{ height:+"+nav_height+"px; } ");
			style_rules.push(".timer-span{ height:+"+timer_height+"px; top:"+timer_top+"px } ");
			// Append css file to html
			style = '<style type="text/css">' + style_rules.join("\n") + "</style>";
			$("head").append(style);
		};

		function animateBrowse(deal){
			// Declare variables
	    	var deal_space = null; // This deals deal space in DOM
			var deal_new_price, deal_prev_price;
			// Find Deal on screen
			$('.deal-space').each(function(){
				if($(this).attr('deal_id') == deal.deal_id){
					deal_space = $(this);
					return false;
				}
			});
			// Get DOM variables from deal-space
			deal_space.removeClass('bidAnimate');
			deal_tile = deal_space.parent();
			deal_new_price = deal_space.find('.price-now');
			deal_prev_price = deal_space.find('.price-prev');
			deal_new_price_cents = deal_new_price.children('.cents');
			deal_prev_price_cents = deal_prev_price.children('.cents');
			console.log("DOM Elemnets");
			console.log(deal_new_price);

			// Update DOM
			deal_new_price.html('&nbsp;$'+Ember.get(deal, 'priceNow')+'<span class="text-small cents">.'+Ember.get(deal, 'priceNowCents')+'</span>');
			deal_prev_price.html('$'+Ember.get(deal, 'pricePrev')+'<span class="text-small cents">.'+Ember.get(deal, 'pricePrevCents')+'</span>');

			// Remove Previous animation classes
			deal_space.removeClass('bidAnimate');
			deal_new_price.css('opacity', 0);
			deal_new_price.removeClass('priceAnimate');

			// Delay for javascript remove class to take effect
			console.log(Ember.get(deal, 'price_now'));
			setTimeout(function(){
				deal_new_price.addClass('priceAnimate');
				deal_space.addClass('bidAnimate');
				// After animation, animate price prev
				setTimeout(function(deal){
					deal.css({'font-weight':'normal', 'text-decoration':'line-through'});
					deal.removeClass('text-orange');
					deal.addClass('text-gray');
				},5000, deal_prev_price);
			}, 50);
	    }
    },
});

/***************************************
	App Router
***************************************/
App.Router.map(function() {
	this.resource('home');
	this.resource('search');
	this.resource('browse');
	this.resource('login');
	this.resource('register');
	this.resource('myprofile');
	this.resource('mypurchases');
	this.resource('feedback');
	this.resource('voucher');
	this.resource('moreinfo', { path: '/more_info/:deal_id' });
	this.resource('purchase', { path: '/purchase/:deal_id' });
});

App.ApplicationController = Ember.Controller.extend({
	currentUser: null,

	/* updateCurrentPath() method updates current & previous state variables
	 * @observes - change in state
	*/
	updateCurrentPath: function() {
		App.set('previousPath', App.get('currentPath'));
        App.set('currentPath', this.get('currentPath'));
        console.log("prev path = " + App.get('previousPath'));
        console.log("current path = " + App.get('currentPath'));
    }.observes('currentPath'),
});

/***************************************
	Index
***************************************/
App.IndexRoute = Ember.Route.extend({
	// redirect() method redirects App on load
	redirect: function() {
		// Declare variables
		var cash = cash_get("search");
		var authData = cash_get("authData");
		var self = this;

		checkLogin();

		// checkLogin() method checks login/auth cash and redirects
		function checkLogin(){
			if(authData == false){
				self.transitionTo('login'); // If no login history, go to login screen
			}
			else{
				// Get user profile info
				console.log("login log authfine");
				authData = jQuery.parseJSON(authData);
				console.log(authData);
				var auth = btoa(authData.token);
				$.ajax({
				    url: server + '/users/' + authData.id,
				    headers:{'Content-Type' : 'application/json', 'Authorization' : 'Basic ' + auth },
				}).done(function(userData){
					self.controllerFor('application').set('currentUser', userData.user);
					// App.set('currentUser.authToken', authData.token);
					console.log("login log currentuser");
					console.log(self.controllerFor('application').get('currentUser'));
					loginInit(authData, userData);
					afterLogin();
				}).fail(function(err){
					self.transitionTo('login');
				});
			}
		}

		// afterLogin() function is called once user logs in from cash
		function afterLogin(){
			// If nothing in cash, go to search
			if(!cash){
	   	 		self.transitionTo('search');
	   	 	}
	   	 	else{
	   	 		console.log("GOT CASH");
	   	 		var now = new Date();
	   	 		cash = jQuery.parseJSON(cash);
	   	 		var bidWindow = new Date(cash.bidwindow);
	   	 		if( (parseInt(cash.others.length) == 0) && (parseInt(cash.results.length) == 0) ){
	   	 			console.log("yes");
	   	 			self.transitionTo('search');
	   	 		}
	   	 		// If bidwindow not over, then transition to browse window
	   	 		else if(now < bidWindow){
	   	 			console.log("GO TO ");
		   	 		console.log(cash.results);
		   	 		deals = cash.results;
		   	 		otherMerchants = cash.others;
		   	 		bidwindow = cash.bidwindow;
		   	 		searchQuery = cash.query_id;
		   	 		self.transitionTo('browse');
		   	 	}
		   	 	else{
		   	 		// Else go back to search
		   	 		self.transitionTo('search');
		   	 	}
	   	 	}
	   	}
  	},
});

App.IndexController = Ember.Controller.extend({
	needs: ['application'],
	currentUser: Ember.computed.alias('controllers.application.currentUser'),
});

/***************************************
	Navigation Header
***************************************/
App.HeaderView =  Ember.View.extend({
	classNames: ['header-view'],
	//Event listener for the side menus
	menuListeners: function(){
		// Declare variables
		var self = this;
		var menu_open = false; //true if main menu open
		var profile_open = false; //true if profile menu open
		var header = $('#header'); //header
		var footer = $('#footer'); //footer
		var main_body = $('.contain'); //main content div
		var menu_main = $('#menu-left'); //main-menu left
		var menu_main_trigger = $('#menu-trigger'); //left main menu trigger
		var menu_profile = $('#menu-right'); //profile menu right
		var menu_profile_trigger = $('#profile-trigger'); //left main menu trigger

		// Event handler for "menu icon" trigger in header
		menu_profile_trigger.hammer().on('touch', function(event){
			// if menu isn't open, then shift menu open
			if(!profile_open){
				menu_profile.css('-webkit-transform', 'translate3d(-100%, 0, 0)');
				header.css('-webkit-transform', 'translate3d(-85%, 0, 0)');
				footer.css('-webkit-transform', 'translate3d(-85%, 0, 0)');
				main_body.css('-webkit-transform', 'translate3d(-85%, 0, 0)');
				main_body.bind("touchmove", function(e){e.preventDefault();});
				profile_open = true;
			}
		});

		// Event handler for menu "swipe"
		menu_profile.hammer().on('swiperight',function(event){
			// if profile menu open, on tap or swiperight close menu
			if(profile_open){
				header.css('-webkit-transform', 'translate3d(0, 0, 0)');
				footer.css('-webkit-transform', 'translate3d(0, 0, 0)');
				main_body.css('-webkit-transform', 'translate3d(0, 0, 0)');
				menu_profile.css('-webkit-transform', 'translate3d(0, 0, 0)');
				main_body.unbind("touchmove");
				profile_open = false;
			}
		});

		// Event handler for on tap of menu tile, close menu and transition to page
		$('.menu-tile').hammer().on('tap', function(event){
			var link = $(this).children('p').text().toLowerCase().replace(/\s+/g,""); // read the text from the paragraph and set as link
			if(profile_open){
				header.css('-webkit-transform', 'translate3d(0, 0, 0)');
				footer.css('-webkit-transform', 'translate3d(0, 0, 0)');
				main_body.css('-webkit-transform', 'translate3d(0, 0, 0)');
				menu_profile.css('-webkit-transform', 'translate3d(0, 0, 0)');
				profile_open = false;
			}
			if(link == "logout"){
				link = "login";
				self.set('controller.currentUser', null);
				cash_remove("authData");
				cash_remove("search");
				resetBrowseData();
				socket_userdata = null;
				socket_logout();
			}
			else if (link == 'ongoingdeals'){
				link = 'browse';
			}
			// then transition to url ending with link
			setTimeout(function(){
				window.location = 'file://' + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + window.location.pathname + '#/' + link;
			}, 10);
		});

		// Event handler for on touch of body when menu open
		main_body.hammer().on('touch', function(event){
			// if profile menu open, then close
			if(profile_open){
				header.css('-webkit-transform', 'translate3d(0, 0, 0)');
				footer.css('-webkit-transform', 'translate3d(0, 0, 0)');
				main_body.css('-webkit-transform', 'translate3d(0, 0, 0)');
				menu_profile.css('-webkit-transform', 'translate3d(0, 0, 0)');
				main_body.unbind("touchmove");
				profile_open = false;
			}
		})
	},
	// Update AZUL logo when current state == search
	updateIcons: function(self){
		if(App.get('currentPath') == "search"){
			$('#home-trigger').removeClass('logo-back');
		}
	},
	// Event listener for AZUL logo touch and logout tile
	touchListeners: function(self){
		var thing = self.get('controller');

		$('#home-trigger').hammer().on('touch', function(event){
			thing.transitionToRoute("search");
		})
		$('#logout-trigger').hammer().on('tap', function(event){
			thing.set('currentUser', null);
			socket_userdata = null;
			socket_logout();
			thing.transitionToRoute('login');
		})
	},
	// Update menu when there are ongoing bids
	ongoingCheck: function(self){
		// Add menu tile if deals[] and ongoingMerchants[] are not empty
		self.set('controller.onGoingBids', ( ((deals.length) != 0) && ((otherMerchants.length)!=0) ) );
	},
	// document.ready for search state
	didInsertElement: function() {
		this.ongoingCheck(this);
		this.updateIcons(this);
		this.menuListeners();
		this.touchListeners(this);
	}
});

App.HeaderController = Ember.Controller.extend({
	needs: ['application'],
	currentUser: Ember.computed.alias('controllers.application.currentUser'),
	onGoingBids: false, 
});

/***************************************
	Login Module
***************************************/
App.LoginView = Ember.View.extend({
	// document.ready touch listeners
	touchListeners: function(self){
		// Transitions to register route on touch of "New user"
		$('#register-trigger').hammer().on('tap', function(event){
			self.get('controller').transitionToRoute('register');
		});
		// Handler for login trigger
		$('#login-trigger').hammer().on('touch release', function(event){
			switch(event.type){
				case 'touch':
					$(this).children().attr("src",'img/icon/forward_w.png');
					break;
				case 'release':
					$(this).children().attr("src",'img/icon/forward.png');
					var thing = self.get('controller');
					var user = {"email": '', "password": ''};
					user.email = thing.get('email');
					user.password = thing.get('password');
					console.log("login log postauth");
					console.log(user);
					var posted = $.post(server + "/authenticate", user);
					posted.done(function(authData){
						console.log("login log authres");
						console.log(authData);
						
						if (authData.hasOwnProperty('err')) {
							console.log("login log authfail");
							alert("Error: " + JSON.stringify(authData.err));
						} else {
							console.log("login log authfine");
							var auth = btoa(authData.token);
							cash_add("authData", JSON.stringify(authData));
							$.ajax({
							    url: server + '/users/' + authData.id,
							    headers:{'Content-Type' : 'application/json', 'Authorization' : 'Basic ' + auth },
							}).done(function(userData){
								thing.set('currentUser', userData.user);
								thing.set('currentUser.authToken', authData.token);
								console.log("login log currentuser");
								console.log(thing.get('currentUser'));
								alert('logged in!');
								loginInit(authData, userData);
								thing.transitionToRoute("search");
							}).fail(function(err){
								alert('Error: ' + JSON.stringify(err));
							});
						}
					}).fail(function(err){
						alert('Error: ' + JSON.stringify(err));
					});
					break;
				}
		});
	},
	// document.ready for login module
	didInsertElement: function(){
		this.touchListeners(this);
	}
});

App.LoginController = Ember.Controller.extend({
	needs: ['application'],
	currentUser: Ember.computed.alias('controllers.application.currentUser'),
	actions:{
		loginfb: function(){
			var self = this;
			FB.init({appId: fbAppId, nativeInterface: CDV.FB, useCachedDialogs: false});
			FB.login(function(res) {
				console.log("login logfb response " + JSON.stringify(res));
				if (res.status == 'connected') {
					// logged into FB; pass info to auth
					var auth = {};
					auth["facebook_id"] = res.authResponse.userId;
					auth["facebook_token"] = res.authResponse.accessToken;
					console.log("login logfb fb signin - " + JSON.stringify(auth));
					var posted = $.post(server + '/authenticate', auth);
					// TODO: posted.done callback is same as the one from log; consider refactoring this out?
					posted.done(function(authData){
						console.log("login logfb postauth response " + JSON.stringify(authData));
						if(authData.hasOwnProperty('err')){
							console.log("login logfb auth fail");
							alert('Error: ' + JSON.stringify(authData.err));
						}else{
							console.log('login logfb auth in');
							var auth = btoa(authData.token);
							$.ajax({
							    url: server + '/users/' + authData.id,
							    headers:{'Content-Type' : 'application/json', 'Authorization' : 'Basic ' + auth },
							}).done(function(userData){
								self.set('currentUser', userData.user);
								self.set('currentUser.authtoken', authData.token);
								
								console.log("login logfb currentuser");
								console.log(self.get('currentUser'));
								alert('logged in!');
								loginInit(authData, userData);
								
								self.transitionToRoute("search");
							}).fail(function(err){
								alert('Error: ' + JSON.stringify(err));
							});
						}
					}).fail(function(err){
						alert('Error: ' + JSON.stringify(err));
					});
				} else {
					alert('FB login failed');
				}
			}, {scope: fbAppScope});
		},
		
		testlogin: function(){
			this.set('currentUser', {email: "emma@gmail.com", id: 55, firstname: "Emilia", lastname: "Robertson"});
			alert('\"logged in\"!');
			this.transitionToRoute("search");
		},
		
		logout: function(){
			this.set('currentUser', null);
			socket_userdata = null;
			socket_logout();
		},
	}
});

/***************************************
	Registration Module
***************************************/
App.RegisterView = Ember.View.extend({
	touchListeners: function(self){
		// Event handler for submitting new profile
		$('#register-trigger').hammer().on('touch release', function(event){
			switch(event.type){
				case 'touch':
					$(this).children().attr("src",'img/icon/forward_w.png');
					break;
				case 'release':
					var thing = self.get('controller');
					$(this).children().attr("src",'img/icon/forward.png');
					var newUser = {"firstname": '', "lastname": '', "email": "", "password": ""};
					newUser.firstname = thing.get('firstname');
					newUser.lastname = thing.get('lastname');
					newUser.email = thing.get('email');
					newUser.password = thing.get('password');
					console.log(newUser);
					
					$.post(server + "/users", newUser, function(userData) {
						if (userData.hasOwnProperty('err')) {
							console.log('register fail');
							alert("Error: " + JSON.stringify(userData));
						} else {
							newUser["id"] = userData.id;
							self.set('currentUser', newUser);
							
							// Authenticate user
							var auth = {};
							auth["email"] = newUser.email;
							auth["password"] = newUser.password;
							
							$.post(server + '/authenticate', auth, function(authData){
								cash_add("authData", JSON.stringify(authData));
								self.set('currentUser.authtoken', authData.token);
								$.ajax({
								    url: server + '/users/' + authData.id,
								    headers:{'Content-Type' : 'application/json', 'Authorization' : 'Basic ' + auth },
								}).done(function(userData){
									self.set('controller.currentUser', userData.user);
									loginInit(authData, userData);
									thing.transitionToRoute('search');
								}).fail(function(err){
									alert("Invalid");
									self.transitionTo('login');
								});
							});
						}
					}).fail(function(err){
						alert('Error: ' + JSON.stringify(err));
					});
					break;
				}
		})
	},
	didInsertElement: function(){
		this.touchListeners(this);
	}
});

App.RegisterController = Ember.Controller.extend({
	needs: ['application'],
	currentUser: Ember.computed.alias('controllers.application.currentUser'),
});

/***************************************
	Search Module
***************************************/
App.SearchRoute = Ember.Route.extend({
	model: function(){
		var min_date = new Date();
		min_date.setMinutes(round_to_thirty(min_date.getMinutes()));
		var datetime = {
			"time": min_date,
			"price": "5",
			"location": {
				"lat": 30.2500,
				"lon": -97.7500,
			}
		};
		return datetime;
	},
	setupController: function(controller, model) {
	    if(cash_get("search-demo")){
	    	controller.set('firstTime', false);
	    }
	    controller.set('model', model);
	}
});

App.SearchView = Ember.View.extend({
	// Handles all hammer touch functionality on doucment.ready (didInsertElement)
	touchListeners: function(self){
		// Declare variables
		var window_width = $('html').width();

		// DOM elements for time
		var slider_time_x = window_width * 0; // Original location of time dot
		var slider_time_width = window_width - slider_time_x;
		var last_time_position = slider_time_x; // last_time_position used to keep track of last position
		var threshold_time = (window_width) / 55; // threshold_time = length of slider * number of 15 min intervals in 23 hrs 45 min (95) +(14/threshold_time * 2) (account for radius of dot) = 109
		var dot_time = $('#slider-dot-time');
		var dot_time_trigger = $('#slide-time-trigger');
		var indicator_time = $('#slider-indicator-time');

		// Create time objects - need to do this before setting indicator_time_text
		indicator_time.html(make_time_text());
		var indicator_time_text = $('.slider-indicator-time-text');

		// Create time objects
		var time_index = 0;
		var time_objs = get_time_text();
		var time_timeout_queue = [setTimeout(show_indicator_text, 0, time_objs[time_index], indicator_time_text)];

		// Slider for time event handler
		dot_time_trigger.hammer({drag_min_distance: 10, hold_timeout: 100}).on("drag", function(event){
			var location = event.gesture.center.pageX;
			if(location > slider_time_x - threshold_time){
				if(location + dot_time.width() < window_width){
					var increment = parseInt((location - last_time_position)/threshold_time);
					$(this).css('-webkit-transform', 'translate3d('+ (location - slider_time_x) +'px, 0, 0)'); // Move dot
					if(time_index + increment < 0){
						time_index = 0;
					}else if(time_index + increment >= time_objs.length){
						time_index = time_objs.length - 1;
					}else{
						time_index += increment;
					}
					last_time_position += increment * threshold_time; // Change current position
					if(time_timeout_queue.length > 3){
						clearTimeout(time_timeout_queue.shift());
						time_timeout_queue.shift();
					}
					time_timeout_queue.push(setTimeout(show_indicator_text, 200, time_objs[time_index], indicator_time_text));
				}
				if(indicator_time.position().left + indicator_time.outerWidth() < slider_time_width || location + indicator_time.outerWidth() < window_width){
					indicator_time.css('-webkit-transform', 'translate3d(' + (location - slider_time_x) + 'px, 0, 0)');
					if(indicator_time.position().left + indicator_time.outerWidth() > slider_time_width){
						indicator_time.css('-webkit-transform', 'translate3d(' + (slider_time_width - indicator_time.outerWidth()) + 'px, 0, 0)');
					}else if(indicator_time.position().left < 0){
						indicator_time.css('-webkit-transform', 'translate3d(0px, 0, 0)');
					}
				}
			}
		});

		// DOM elements for price
		var slider_price_x = window_width * 0.02; // Original location of price dot
		var last_price_position = slider_price_x;
		var threshold_price = window_width / 4.47; // threshold_time = length of slider / number of 5 dollar intervals in 100 +(14/threshold_time * 2) (account for radius of dot) = 109
		var dot_price = $('#slider-dot-price');
		var dot_price_trigger = $('#slide-price-trigger');
		var indicator_price = $('#slider-indicator-price');

		// Create price objects - need to do this before setting indicator_time_text
		indicator_price.html(make_price_text());
		var indicator_price_text = $('.slider-indicator-price-text');

		// Create price objects
		var price_index = 0;
		var price_objs = get_price_text();
		var price_timeout_queue = [setTimeout(show_indicator_text, 0, price_objs[price_index], indicator_price_text)];

		// Slider for price event handler
		dot_price_trigger.hammer({drag_min_distance: 10}).on("drag", function(event){
			var location = event.gesture.center.pageX;
			if(location > 0){
				if(location-(slider_price_x/2) + dot_price.width() < window_width){
					var increment = parseInt((location - last_price_position)/threshold_price);
					$(this).css('-webkit-transform', 'translate3d('+ (location - slider_price_x) +'px, 0, 0)');
					if(price_index + increment < 0){
						price_index = 0;
					}else if(price_index + increment >= price_objs.length){
						price_index = price_objs.length - 1;
					}else{
						price_index += increment;
					}
					last_price_position += increment * threshold_price;
					if(price_timeout_queue.length > 5){
						clearTimeout(price_timeout_queue.shift());
						price_timeout_queue.shift();
					}
					price_timeout_queue.push(setTimeout(show_indicator_text, 200, price_objs[price_index], indicator_price_text));
				}
				if(indicator_price.position().left + indicator_price.outerWidth() < window_width || location + indicator_price.outerWidth() < window_width){
					indicator_price.css('-webkit-transform', 'translate3d(' + (location - slider_price_x) + 'px, 0, 0)');
					if(indicator_price.position().left + indicator_price.outerWidth() > window_width){
						indicator_price.css('-webkit-transform', 'translate3d(' + (window_width - indicator_price.outerWidth()) + 'px, 0, 0)');
					}
				}
			}
		});

		// Event handler for "touch" of AZUL search button
		$('#search-trigger').hammer().on('touch', function(event){
			$('#search-trigger').html('<img src="img/loader.gif" style="width: 200%; position: relative; left: -60%; margin-top: 3%" />');
			if(!(self.get('controller.firstTime'))){
				// Define variables
				var date_time = new Date(self.get('controller.dateYear'), self.get('controller.dateMonth'), self.get('controller.dateDay'));
				var indicator_time = $('#slider-indicator-time').children('.slider-indicator-time-text').filter(function(){return ($(this).css('visibility') == 'visible')});
				var lat = self.get('controller.model.location.lat');
				var lon = self.get('controller.model.location.lon');
				var search_q = self.get('controller.model.search_query');
				var user_id = self.get('controller.currentUser.id');
				var now = new Date();
				var hour_time = get_selector_time(indicator_time);

				// Update date time to include indicator hours & minutes
				date_time.setMinutes(hour_time.getMinutes());
				date_time.setHours(hour_time.getHours());
				bidwindow = date_time;

				// Get location select
				if($('#location-selector option:selected').val() == 'austin_tx'){
					lat = 30.2500;
					lon = -97.7500;
				}
				// Search query
				var search_data = {
					"type":"deal",
					"data":{
						"search_query": search_q, // search text
						"user_id": user_id,
						"date_time": rfc3339_dt(date_time),
						"location": {
							"lat": lat,
							"lon": lon,
						},
						"distance": parseInt($('#search-distance').val()),
						"price": 20
					}
				};

				// Send search query to backend
				if(user_id == null){
					alert("Not logged in!");
					self.get('controller').transitionToRoute('login'); // If user not logged in, transition to login state
				}else if (date_time < now){
					console.log("Invalid bidwindow");
					// If invalid bid window given, reset browse data and proceed
					resetBrowseData();
					setTimeout(function(){
						alert("Invalid Bid Window");
						self.get('controller').transitionToRoute('browse');
					}, 1200);
				}else{
					socket_initialize(); // Re-initialize socket
					var posted = $.post(server + "/search", search_data); // Post to search endpoint
					// On success, update global variables and proceed to browse deals
					posted.done(function(searchRes){
						// Update Globals
						deals = parseDeals(searchRes.results);
						otherMerchants = searchRes.others;
						searchQuery = searchRes.query_id;
						
						deals.sort(function(a,b){if(a.merchant == b.merchant)return 0;else if(a.merchant > b.merchant)return 1;else{return -1;}}); // Sort deals alphabetically by merchant
						searchRes["bidwindow"] = date_time;
						cash_add("search", JSON.stringify(searchRes)); // Cache the search results
						App.set('bidDelay', false); // Reset fake bid delay
						setTimeout(function(){
							self.get('controller').transitionToRoute('browse');
						}, 1200);
					});
					posted.fail(function(res) {
						resetBrowseData(); // Reset browse data
						setTimeout(function(){
							self.get('controller').transitionToRoute('browse');
						}, 1200);
					});
				}
			}
			else{
				$('.demo-contain').css('opacity', 0);
				setTimeout(function(){
					self.set('controller.firstTime', false);
				},500);
			}
		});

		// Event handler for for demo screen if firstTime
		if(self.get('controller.firstTime')){
			$('.demo-contain').hammer().on('touch', function(event){
				$(this).css('opacity', 0);
				setTimeout(function(){
					self.set('controller.firstTime', false);
					cash_add("search-demo", true);
				},600);
			})
		}
	},
	// Finds user location using phonegap "geolocation plugin"
	findLocation: function(self){
		navigator.geolocation.getCurrentPosition(onGeoSuccess, onGeoError); // Find location at start of search view
		function onGeoSuccess (position){
			// If location found, set location
			self.set('controller.model.location.lat', position.coords.latitude);
			self.set('controller.model.location.lon', position.coords.longitude);
		}
		function onGeoError (position){
			console.log("Couldn't find location");
		}

	},
	// Initializes custom date picker
	DatePicker: function(self){
		// Set Date Variables
		var today = new Date();
		var month = today.getMonth();
		var date = today.getDate();
		var year = today.getFullYear();

		// Get Elements from DOM
		var search_date = $('#search-date');
		var date_picker = $('#datePicker-contain');
		var set_btn = $('#setTime');
		var cancel_btn = $('#cancelTime');
		var add_month = $('#addMonth');
		var add_day = $('#addDay');
		var add_year = $('#addYear');
		var curr_month = $('#currMonth');
		var curr_day = $('#currDay');
		var curr_year = $('#currYear');
		var sub_month = $('#subMonth');
		var sub_day = $('#subDay');
		var sub_year = $('#subYear');

		var open = false;
		var hold_interval;
		var this_month = month;
		var this_day = date;
		var this_year = year;

		var months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]; // Month by abreviation
		var days = [31,28,31,30,31,30,31,31,30,31,30,31]; // Num of days in each month

		self.set('controller.dateYear', year);
		self.set('controller.dateMonth', month);
		self.set('controller.dateDay', date);
		search_date.val(months[month] +' '+ date +', '+ year); // Set input to today's Date
		// Set DatePicker to today's date
		curr_month.text(months[this_month]);
		curr_day.text(this_day);
		curr_year.text(this_year);
		date_picker.hide();

		// Listener for user opening DatePicker
		search_date.hammer().on('tap', function(event){
			if(!open){
				date_picker.show(); // Show datepicker
				open = true; // Open datepicker
				setTimeout(function(){
					date_picker.css('opacity', '1'); // Set opacity
				}, 50);
			}
		});
		function onTouch(self){
			self.addClass('bg-light-gray');
			self.removeClass('bg-beige');
		};
		function offTouch(self){
			self.addClass('bg-beige');
			self.removeClass('bg-light-gray');
		};
		function clearInt(){
			window.clearInterval(hold_interval);
		}
		function increaseMonth(){
			this_month++;
			if(this_month == months.length){
				this_month = 0; // Set to JAN if DEC
			}
			if(this_day > days[this_month]){
				this_day = days[this_month];
				curr_day.text(this_day);
			}
			curr_month.text(months[this_month]);
		};
		function decreaseMonth(){
			this_month--;
			if(this_month < 0){
				this_month = months.length-1; // Set to JAN if DEC
			}
			if(this_day > days[this_month]){
				this_day = days[this_month];
				curr_day.text(this_day);
			}
			curr_month.text(months[this_month]);
		};
		function increaseDay(){
			$(this).removeClass('bg-beige');
			this_day++;
			if(this_day > days[this_month]){
				this_day = 1; // Set to JAN if DEC
			}
			curr_day.text(this_day);
		};
		function decreaseDay(){
			this_day--;
			if(this_day < 1){
				this_day = days[this_month]; // Set to JAN if DEC
			}
			curr_day.text(this_day);
		};
		function increaseYear(){
			this_year++;
			if((this_year % 4) == 0){
				days[1] = 29;
			}
			else{
				days[1] = 28;
				if(this_day > days[this_month]){
					this_day = days[this_month];
					curr_day.text(this_day);
				}
			}
			curr_year.text(this_year);
		};
		function decreaseYear(){
			if(this_year > 0){
				this_year--;
				if((this_year % 4) == 0){
					days[1] = 29;
				}
				else{
					days[1] = 28;
					if(this_day > days[this_month]){
						this_day = days[this_month];
						curr_day.text(this_day);
					}
				}
				curr_year.text(this_year);
			}
		};
		// Listeners for changing month
		add_month.add(sub_month).hammer({hold_threshold: 2, hold_timeout: 250}).on('touch hold release', function(event){
			var thing = $(this);
			var id = event.target.id;
			switch(event.type){
				case 'hold':
					hold_interval = setInterval(function(){
						if(id == 'addMonth')increaseMonth();
						else decreaseMonth();
					}, 200);
					break;
				case 'touch':
					onTouch(thing);
					if(id == 'addMonth')increaseMonth();
					else decreaseMonth();
					break;
				case 'release':
					offTouch(thing);
					clearInt();
					break;
			}
		});

		// Listeners for changing day
		add_day.add(sub_day).hammer({hold_threshold: 2, hold_timeout: 250}).on('touch hold release', function(event){
			var thing = $(this);
			var id = event.target.id;
			switch(event.type){
				case 'hold':
					hold_interval = setInterval(function(){
						if(id == 'addDay')increaseDay();
						else decreaseDay();
					}, 100);
					break;
				case 'touch':
					onTouch(thing);
					if(id == 'addDay')increaseDay();
					else decreaseDay();
					break;
				case 'release':
					offTouch(thing);
					clearInt();
					break;
			}
		});

		// Listeners for changing year
		add_year.add(sub_year).hammer({hold_threshold: 2, hold_timeout: 250}).on('touch hold release', function(event){
			var thing = $(this);
			var id = event.target.id;
			switch(event.type){
				case 'hold':
					hold_interval = setInterval(function(){
						if(id == 'addYear')increaseYear();
						else decreaseYear();
					}, 200);
					break;
				case 'touch':
					onTouch(thing);
					if(id == 'addYear')increaseYear();
					else decreaseYear();
					break;
				case 'release':
					offTouch(thing);
					clearInt();
					break;
			}
		});

		// Listener for set button
		set_btn.hammer().on('touch', function(event){
			self.set('controller.dateYear', this_year);
			self.set('controller.dateMonth', this_month);
			self.set('controller.dateDay', this_day);
			search_date.val(months[this_month] +' '+ this_day +', '+ this_year); // Set input to Date
			// Close date picker
			date_picker.css('opacity', '0');
			setTimeout(function(){
				date_picker.hide();
				open = false;
			}, 500);
		});

		// Listener for cancel button
		cancel_btn.hammer().on('touch', function(event){
			// Close date picker
			date_picker.css('opacity', '0');
			setTimeout(function(){
				date_picker.hide();
				open = false;
				// Reset variables + input
				var string = search_date.val().replace(',', '').split(" ");
				this_month = months.indexOf(string[0]);
				this_day = string[1];
				this_year = string[2];
				curr_month.text(months[this_month]);
				curr_day.text(this_day);
				curr_year.text(this_year);
			}, 500);
		});

	},
	// doucment.ready for search state
	didInsertElement: function(){
		var self = this;
		checkLogin(this);
		this.findLocation(self);
		this.DatePicker(self);
		this.touchListeners(self);
	}
});
App.SearchController = Ember.ObjectController.extend({
	needs: ['application'],
	currentUser: Ember.computed.alias('controllers.application.currentUser'),
	firstTime: true,
	dateMonth: '',
	dateDay: '',
	dateYear: '',
});


/***************************************
	Browse Module
***************************************/

// View for browse navigation parent view
App.BrowseView = Ember.View.extend({
	touchListeners: function(self){
		// Declare variables
		var favorite_btn = $('#browse-footer-favorite-trigger');
		var sort_btn = $('#browse-footer-sort-trigger');
		var map_btn = $('#browse-footer-map-trigger');

		// Event listener for favorite icon
		favorite_btn.hammer().on('touch', function(event){
			var thing = $(this).children('.favorite-icon');
			if(!self.get('controller.firstTime')){
				var flag = !self.get('controller.favFlag');
				self.set('controller.favFlag', flag);
				self.set('controller.sortFlag', false);
				self.set('controller.mapFlag', false);
				if(flag){
					thing.css('content', 'url("img/icon/favorite_w.png")');
					sort_btn.children('.icon').css('content', 'url("img/icon/sort.png")');
					map_btn.children('.icon').css('content', 'url("img/icon/map.png")');
				}else{
					thing.css('content', 'url("img/icon/favorite.png")');
				}
				self.set('controller.content', deals);
				self.set('controller.otherMerchants', otherMerchants.slice(0,10));
			}
		});

		// Event listener for sort icon
		sort_btn.hammer().on('touch', function(event){
			var thing = $(this).children('.sort-icon');
			if(!self.get('controller.firstTime')){
				var flag = !self.get('controller.sortFlag');
				self.set('controller.favFlag', false);
				self.set('controller.sortFlag', flag);
				self.set('controller.mapFlag', false);
				if(flag){
					thing.css('content', 'url("img/icon/sort_w.png")');
					favorite_btn.children('.icon').css('content', 'url("img/icon/favorite.png")');
					map_btn.children('.icon').css('content', 'url("img/icon/map.png")');
				}else{
					thing.css('content', 'url("img/icon/sort.png")');
				}
				self.set('controller.content', deals);
				self.set('controller.otherMerchants', otherMerchants.slice(0,10));
			}
		});

		// Event listener for map icon
		map_btn.hammer().on('touch', function(event){
			var thing = $(this).children('.map-icon');
			if(!self.get('controller.firstTime')){
				var flag = !self.get('controller.mapFlag');
				self.set('controller.favFlag', false);
				self.set('controller.sortFlag', false);
				self.set('controller.mapFlag', flag);
				if(flag){
					thing.css('content', 'url("img/icon/map_w.png")');
					favorite_btn.children('.icon').css('content', 'url("img/icon/favorite.png")');
					sort_btn.children('.icon').css('content', 'url("img/icon/sort.png")');
				}else{
					thing.css('content', 'url("img/icon/map.png")');
				}
				self.set('controller.content', deals);
				self.set('controller.otherMerchants', otherMerchants.slice(0,10));
			}
		});
	},
	// updateNavigation checks state when reloading browsedeals and changes navigation accordingly
	updateNavigation: function(self){
		if(self.get("controller.favFlag")){
			$('#browse-footer-favorite-trigger').children('.icon').css('content', 'url("img/icon/favorite_w.png")');
		}else if(self.get("controller.sortFlag")){
			$('#browse-footer-sort-trigger').children('.icon').css('content', 'url("img/icon/sort_w.png")');
		}else if(self.get("controller.mapFlag")){
			$('#browse-footer-map-trigger').children('.icon').css('content', 'url("img/icon/map_w.png")');
		}
	},
	// setTimer handles bid window timer
	setTimer: function(self){
		// Declare variables
	    // Get reference to the interval doing the countdown
	    var now = new Date();
	    var end = new Date(bidwindow);
	    // var end = (new Date()).setSeconds(now.getSeconds()+5);
	   	var duration = parseInt((end - now)/1000) + 2;
	    var temp = 0;
	    var days = 0, hours = 0, minutes = 0, seconds = 0;
	    var time = duration;

	    // Set Time 
	    if(time >= 0){
		    temp = time;
	    	days = parseInt( (temp / 86400),10);
	    	temp %= 86400;
	    	hours = parseInt( (temp / 3600),10);
	    	temp %= 3600;
	    	minutes = parseInt( (temp / 60 ) );
	    	temp %= 60;
	    	seconds = temp;
	    	if(days == 0){
	    		$('#timer').removeClass('text-large');
		        $('#timer').addClass('text-xxlarge');
		        $('.timer-span').css({'height':$('html').height()*.06, 'top':$('html').height()*0.015});
		    }
	        $('#timer').text(((days == 0)?'': (days == 1)?'1 day ': days + ' days ') + ((hours<10)?'0':'') + hours + ':' + ((minutes<10)?'0':'') + minutes + ':' + ((seconds<10)?'0':'') + seconds);
	    }
        // Clear previous intervals
        for (var i = 1; i < 99999; i++)window.clearInterval(i);

	    // Set interval to countdown every second
		if(time >= 0){
		    countdown = setInterval(function () {
		        // If seconds remain
		        if (--time > 0) {
		        	timerCountDown();  	
		        // Otherwise
		        }else {
		            // Clear the countdown interval
		            $('#timer').text('00:00:00');
		            clearInterval(countdown);
		            timerEnd(self);
		            // And fire the callback passing our container as `this` 
		        }
		    // Run interval every 1000ms (1 second)
		    }, 1000);
		}

		// timerCountDown() counts down timer and updates DOM
		function timerCountDown(){
			// Reset timer on reload
			if(refreshTime){
	    		var now = new Date();
			    var end = new Date(bidwindow);
			    // var end = (new Date()).setSeconds(now.getSeconds()+5);
	    		time = parseInt((end - now)/1000) + 2;
	    		refreshTime = false;
	    	}
        	temp = time;
        	days = parseInt( (temp / 86400),10);
        	temp %= 86400;
        	hours = parseInt( (temp / 3600),10);
        	temp %= 3600;
        	minutes = parseInt( (temp / 60 ) );
        	temp %= 60;
        	seconds = temp;
        	if( (days == 0) && (hours==23) && (minutes==59)){
	    		$('#timer').removeClass('text-large');
		        $('#timer').addClass('text-xxlarge');
		        $('.timer-span').css({'height':$('html').height()*.06, 'top':$('html').height()*0.015});
		    }
            // Update our container's message
            $('#timer').text(((days == 0)?'':(days == 1)?'1 day ': days + ' days ') + ((hours<10)?'0':'') + hours + ':' + ((minutes<10)?'0':'') + minutes + ':' + ((seconds<10)?'0':'') + seconds);
		}

		// timerEnd() method called when bid window ends
		function timerEnd(self){
			alert("Bid Window Over");
			resetBrowseData();
			window.location = 'file://' + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + window.location.pathname + '#/search';
		}
	},
	// updateModel resets all flags
	updateModel: function(self){
		self.set('controller.favFlag', false);
		self.set('controller.sortFlag', false);
		self.set('controller.mapFlag', false);
	},
	// document.ready for browse navigation
	didInsertElement: function(){
		checkLogin(this);
		this.setTimer(this);
		this.updateModel(this);
		this.updateNavigation(this);
		this.touchListeners(this);
	}
});

// View for browse tiles list
App.BrowsetilesView = Ember.View.extend({
	classNames: ['tileView'],
	itemController: 'item',
	dragListeners: function(self){
		// Delcare variables
		var window_width = $('html').width();
		var body = $('.contain');
		var speed = 0;

		// Event handler for deal space interaction
		$('.deal-space').hammer({tap_max_touchtime: 150}).on("tap dragleft dragright dragend", function(event){
			var thing = $(this);
			if(body.position().left == 0){
				switch(event.type){
					case 'tap':
						console.log(event.target.id);
						if( (event.target.id != "fav-btn-trigger")&& (event.target.id != "buy-btn") && (event.target.id != "fav-btn")){
							console.log("yippe");
							window.location = 'file://' + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + window.location.pathname + '#/more_info/' + thing.attr('deal_id');
						}
						else if((event.target.id == "fav-btn-trigger")||(event.target.id == "fav-btn")){
							// Else it is a favorite
							var id = $(this).attr('deal_id');
							var deal = deals.findBy('deal_id', id);
							var fav = Ember.get(deal, 'favorite');
							if(!fav){
								$(this).find('#fav-btn').attr("src",'img/btn/fav_btn_thick.png');
							}
							else{
								$(this).find('#fav-btn').attr("src",'img/btn/fav_btn.png');
							}
							Ember.set(deal, 'favorite', !fav);
							if(self.get('controller.favFlag')){
								$(this).parent('.deal-tile').remove();
							}
							// Update cache
							updateCache();
							// Send favorite via socket io
							socket_sendFav({id: Ember.get(deal, 'deal_id'), fav: !fav});
						}
						else if(event.target.id == 'buy-btn'){
							window.location = 'file://' + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + window.location.pathname + '#/purchase/' + thing.attr('deal_id');
						}
						break;
					case 'dragleft':
					case 'dragright':
						var delta = event.gesture.deltaX;
						speed = event.gesture.velocityX;
						body.bind("touchmove", function(e){e.preventDefault();}); // Prevent body from scrolling vertically
						thing.css("-webkit-transform", 'translate3d('+ delta +'px, 0px, 0px)');
						break;
					case 'dragend':
						var midScreen = window_width/2; // Midpoint theshold
						var offsets = $(this).offset();
						var direc = event.gesture.direction;
						var deleteSibling = thing.siblings( ".delete");
						body.unbind("touchmove");
						thing.addClass('tile-slide-fast');
						if( ((speed > 0.5)&&(direc == "right")) || (offsets.left > midScreen) ){
							thing.css('-webkit-transform', 'translate3d('+(window_width)+'px, 0px, 0px)');
							setTimeout(function(){
								thing.removeClass('tile-slide-fast');
							}, 450);
							deleteSibling.addClass('fadeIn');
						}
						else if( ((speed > 0.5)&&(direc == "left")) || (offsets.left < (-1*midScreen)) ){
							thing.css('-webkit-transform', 'translate3d('+(-1*window_width)+'px, 0px, 0px)');
							setTimeout(function(){
								thing.removeClass('tile-slide-fast');
							}, 450);
							deleteSibling.addClass('fadeIn');
						}
						else {
							thing.removeClass('tile-slide-fast');
							thing.css('-webkit-transform', 'translate3d(0px, 0px, 0px)');
						}
						break;
					}
				}
		});

		// Event handler for on release of "undo" button
		$('.undo-span').hammer().on("release", function(event){
			// Declare variables
			var thing = $(this);
			var tile = thing.parent().siblings( ".deal-space" );

			// If menu !open, undo
			if(body.position().left == 0){
				tile.addClass('tile-slide-slow');
				thing.parent().removeClass('fadeIn');
				setTimeout(function(){
					tile.removeClass('tile-slide-slow');
				}, 500);
				tile.css('-webkit-transform', 'translate3d(0px, 0px, 0px)');
			}
		});

		// Event handler for on release of "delete" button
		$('.delete-button').hammer().on('release', function(event){
			// Declare variables
			var tile = $(this).parent().parent(); // Get the tile associated with this delete button
			var deal = deals.findBy('deal_id', $(this).parent().siblings('.deal-space').attr('deal_id'));
			var index = deals.indexOf(deal);

			deals = deals.slice(0, index).concat(deals.slice(index+1, deals.length)); // Remove deal
			updateCache(); // Update cache
			socket_sendTrash({id: Ember.get(deal, 'deal_id')}); // Send to socket

			// If menu !open, delete
			if(body.position().left == 0){
				setTimeout(function(){
					tile.remove();
					console.log(deals.filterBy('favorite').length);
					if( (deals.filterBy('favorite').length  == 0) && (self.get('controller.favFlag')) ) {
						self.set('controller.model', deals);
					}
					else if(deals.length == 0){
						self.set('controller.model', deals);
					}
				}, 400);
			}
		});

		// Event handler for tap of "add" button on other merchant tiles
		$('.add-btn').hammer().on('tap', function(event){
			// Declare variables
			var thing = $(this);
			var tile = thing.parents('.otherMerchant-space');
			var merchant = otherMerchants.findBy('merchant_id', tile.attr('merchant_id'));
			var thankYou = tile.siblings('.otherMerchant-thankyou');
			var merchantTile = tile.parent('.deal-tile');
			var index = otherMerchants.indexOf(merchant);

			otherMerchants = otherMerchants.slice(0, index).concat(otherMerchants.slice(index+1, otherMerchants.length)); // Remove otherMerchant
			
			// Animate Tile to left, display message, delete tile
			tile.addClass('tile-slide-slow');
			tile.css('-webkit-transform', 'translate3d('+(-1*window_width)+'px, 0px, 0px)');
			setTimeout(function(){
				tile.removeClass('tile-slide-slow');
			}, 500);
			thankYou.addClass('fadeIn');
			setTimeout(function(){
				// Remove tile
				merchantTile.remove();
			}, 5000);

			socket_sendSuggest(merchant); // Send merchant via socket io
		});
	},
	// setStars() method updates rating stars
	setStars: function(){
		$('span.stars-gray').stars();
	},
	// parseDeals() updates deals price prev and price now DOM elements
	parseDeals: function(){
		// For each deal on load, check if deal has been bid on to display 2 prices instead of one
		$('.price-prev').each(function(){
			var price_now = $(this).siblings('.price-now');
			if($(this).text() != price_now.text().trim() ){
				$(this).css({'font-weight':'normal', 'text-decoration':'line-through'});
				$(this).removeClass('text-orange');
				$(this).addClass('text-gray');
				price_now.css('opacity', '1');
			}
		});
	},
	// browse demo function
	demo: function(self){
		// Declare variables
		var demo_tile = $('.deal-space:eq(1)');
		var timeouts = [];
		var deleteInterval;
		// Clear all time intervals
        for (var i = 1; i < 9999; i++){
        	if(i != countdown){
       			window.clearInterval(i);
       		}
       	}
       	// Hide DOM elements
       	$('#dot-slide-delete').hide();
       	$('#dot-slide-delete-undo').hide();

       	// If model > length 1, start swipe animation
       	if(self.get('controller.model').length > 1){
			// Animation for swip to delete
			var startBrowseInfo = setTimeout(function(){
				swipeDeleteDemo();
				deleteInterval = setInterval(swipeDeleteDemo, 5250);
			}, 1000);
			function swipeDeleteDemo(){
				$('#dot-slide-delete').show();
				$('#dot-slide-delete-undo').hide();
				$('#dot-slide-delete').parent().addClass('dotSwipeToDeleteDot');
				demo_tile.addClass('swipeToDeleteTile');
				timeouts = [];

				timeouts.push(setTimeout(function(){
					demo_tile.css('-webkit-transform', 'translate3d(100%,0,0)');
				}, 250));
				timeouts.push(setTimeout(function(){
					$('#dot-slide-delete').hide();
				}, 500));
				timeouts.push(setTimeout(function(){
					demo_tile.siblings('.delete').addClass('fadeIn');
				}, 1250));
				timeouts.push(setTimeout(function(){
					$('#dot-slide-delete-undo').show();
					$('#dot-slide-delete').parent().removeClass('dotSwipeToDeleteDot');
				}, 2750));
				timeouts.push(setTimeout(function(){
					$('#dot-slide-delete-undo').hide();
					demo_tile.css('-webkit-transform' ,'translate3d(0,0,0)');
				}, 3250));
				timeouts.push(setTimeout(function(){
					demo_tile.siblings('.delete').removeClass('fadeIn');
				}, 4250));
			}
		}
		// Event handler for demo screen
		$('.demo-contain').hammer().on('touch', function(event){
			clearTimeout(startBrowseInfo);
			clearInterval(deleteInterval);
			for(var i = 0, z = timeouts.length; i < z; i++){
       			clearTimeout(timeouts[i]);
       		}
			demo_tile.removeClass('swipeToDeleteTile');
			demo_tile.siblings('.delete').removeClass('fadeIn fadeOut');
			setTimeout(function(){
				demo_tile.css('-webkit-transform' ,'translate3d(0,0,0)');
			},100);
			$(this).css('opacity', 0);
			setTimeout(function(){
				self.set('controller.firstTime', false);
				cash_add('browse-demo', true);
				self.dragListeners(self);
			},500);
		});
	},
	// setFavs updates all favorite deals
	setFavs:function(self){
		var deals = self.get("controller.model");
		$('.deal-space').each(function(){
			var id = $(this).attr('deal_id');
			var deal = deals.findBy('deal_id', id);
			if(deal.favorite){
				$(this).find('#fav-btn').attr("src",'img/btn/fav_btn_thick.png');
			}
			else{
				$(this).find('#fav-btn').attr("src",'img/btn/fav_btn.png');
			}
		});
	},
	// renderDeals renders list of deals & otherMerchants to DOM
	renderDeals: function(self){
		// Declare variables
		var html = '';
		var temp_deals = [];
		// If sort flag, render sort deals
		if(self.get('controller.sortFlag')){
			temp_deals = deals.slice(0);
			temp_deals.sort(function(a,b){return a.price_now - b.price_now;});
		} else if(self.get('controller.favFlag')){
			for(var x = 0; x < deals.length; x++){
				if(Ember.get(deals[x], 'favorite')){
					temp_deals.push(deals[x]);
				}
			}
		} else {
			temp_deals = deals.slice(0);
		}
		for(var x = 0; x < temp_deals.length; x++){
			html += '<div class="col-xs-12 clear tile deal-tile"><div class="delete"><div class="col-xs-4 clear delete-span undo-span"><p class="text-beige text-xlarge">Undo</p></div><div class="col-xs-8 clear delete-span delete-button"><p class="text-beige text-xlarge text-right">Delete</p></div></div><div class="deal-space bg-beige" deal_id="'+Ember.get(temp_deals[x], 'deal_id')+'"><div class="col-xs-4 clear deal-tile-img-div"><img src="'+Ember.get(temp_deals[x], 'img')+'"class="deal-tile-img"></div><div class="col-xs-8 clear deal-tile-info voffset2-5"><div class="col-xs-12 clear deal-tile-info-price"><span class="text-gray clear text-12xlarge avenir-light">$'+Ember.get(temp_deals[x], 'price_reg')+' for <span class="price-prev text-orange">$'+Ember.get(temp_deals[x], 'pricePrev')+'<span class="text-small cents">.'+Ember.get(temp_deals[x], 'pricePrevCents')+'</span></span><span class="price-now text-orange"> $'+Ember.get(temp_deals[x], 'priceNow')+'<span class="text-small cents">.'+Ember.get(temp_deals[x], 'priceNowCents')+'</span></span></span></div></div><div class="col-xs-8 clear deal-tile-info"><div class="col-xs-9 clear deal-tile-info-text"><span class="text-uppercase text-gray text-medium clear merchant-name">'+Ember.get(temp_deals[x], 'merchant')+'</span><br/><div class="text-gray text-small clear categories">'+Ember.get(temp_deals[x], 'category')+'</div><span class="stars-gray" rating="'+Ember.get(temp_deals[x], 'rating')+'"></span><br/><span class="text-gray text-small clear">'+Ember.get(temp_deals[x], 'distance')+' mi</span></div><div class="col-xs-3 clear deal-tile-buttons"><div class="text-center" id="fav-btn-trigger"><img class="fith25 icon favorite-icon" id="fav-btn" src="img/btn/fav_btn.png"></div><div class="col-xs-12 clear"><button type="button" class="btn button-rounded text-large" id="buy-btn">BUY</button></div></div></div></div></div>';
		}
		if(!self.get('controller.favFlag')){
			for(var x = 0; x < otherMerchants.length; x++){
				if (x > 9) {
					break;
				}
				html += '<div class="tile deal-tile"><div class="otherMerchant-thankyou"><div class="col-xs-12 clear thankyou-span"><p class="text-beige text-large">Thanks for suggesting we add ' + Ember.get(otherMerchants[x], 'name')+ ' to Azul. We will be sure to invite them to join</p></div></div><div class="otherMerchant-space bg-beige" merchant_id="'+Ember.get(otherMerchants[x], 'merchant_id')+'"><div class="col-xs-4 clear deal-tile-img-div"><img src="img/no_restaurant.png" class="deal-tile-img"></div><div class="col-xs-8 clear deal-tile-info"><div class="col-xs-9 clear voffset5 purchase-tile-info-text"><span class="text-uppercase text-gray text-medium clear merchant-name">'+Ember.get(otherMerchants[x], 'name')+'</span><br /><span class="text-gray text-small clear">'+Ember.get(otherMerchants[x],'category') +'</span><br /><span class="stars-gray" rating="' + Ember.get(otherMerchants[x],'rating') +'"></span><br /><span class="text-gray text-small clear"> '+Ember.get(otherMerchants[x],'distance') +' mi</span></div><div class="col-xs-3 clear deal-tile-buttons voffset30"><div class="deal-tile-button"><button type="button" class="btn button-gray button-rounded text-large add-btn" id="buy-btn">Add</button></div></div></div></div></div>';
			}
		}
		$('.tileView').append(html);
		// Update deal space inventory
		$('.deal-space').each(function(){
			var deal = deals.findBy('deal_id', $(this).attr('deal_id'));
			var inventory = Ember.get(deal, 'inventory');
			var buy_btn = $(this).find('#buy-btn');
			$(this).parent('.deal-tile').css('-webkit-transform', 'translate3d(0px,0px,0px)'); // translate for animations
			// Update inventory
			if(inventory > 5){
				buy_btn.addClass('button-green');
			}else if(inventory >= 3){
				buy_btn.addClass('button-yellow');
			}else{
				buy_btn.addClass('button-red');
			}
		});
		// Update merchant font size
		$('.merchant-name').each(function(){
			var merchant_name = new String($(this).text());
			var num_words = merchant_name.split(" ").length;
			var word_length = merchant_name.length;
			if( (num_words == 2) && (word_length > 15) ){
				$(this).removeClass('text-medium');
				$(this).addClass('text-small');
			}else if( (num_words == 3) && (word_length > 18)){
				$(this).removeClass('text-medium');
				$(this).addClass('text-small');
			}else if (num_words >=4){
				$(this).removeClass('text-medium');
				$(this).addClass('text-small');
			}
		})
	},
	// document.ready for browse tiles view
	didInsertElement: function(){
		deals = parseDeals(deals);
		this.renderDeals(this);
		this.setStars();
		this.parseDeals();
		this.setFavs(this);
		if(this.get('controller.firstTime')){
			this.demo(this);
		}
		else{
			this.dragListeners(this);
		}
	}
});

App.BrowseRoute = Ember.Route.extend({
	model: function(){
		return deals;
	}.property('deals.@each'),
	setupController: function(controller, model) {
	    controller.set('model', deals);
	    controller.set('otherMerchants', otherMerchants);
	    if(cash_get("browse-demo")){
	    	controller.set('firstTime', false);
	    }
	}
});

App.BrowseController = Ember.ArrayController.extend({
	needs: ['application'],
	currentUser: Ember.computed.alias('controllers.application.currentUser'),
	itemController: 'item',
	sortProperties: ['merchant'],
  	sortAscending: true,
	mapFlag: false,
	favFlag: false,
	sortFlag: false,
	firstTime: true,
	// True if user favorites something
	hasFavorite: function(){
		return !(this.get('model').filterBy('favorite').get('length') > 0);
	}.property('@each.favorite'),
	nullSearch: function(){
		return (this.get('model').length == 0 && this.get('otherMerchants').length ==0);
	}.property('@each'),
});

App.ItemController = Ember.ObjectController.extend({
	isFavorite: function(){
		return this.get('favorite');
	}.property('favorite'),
});

/***************************************
	Browse Map
***************************************/
App.BrowsemapView = Ember.View.extend({
	classNames: ['mapView'],
	// Adds each deal location to the map view
	addMap: function(map){
		for(var x=0; x<deals.length; x++){
			var lat = deals[x].location.lat;
			var lon = deals[x].location.lon;
			console.log("merchant = " + deals[x].merchant);
			console.log("lat = " + lat + ", lon = " + lon);
			var marker = new L.Marker([lat, lon]).addTo(map);
			marker.bindPopup('<div class="col-xs-5 clear map-img-div"><img src="'+ deals[x].img +'" class="deal-tile-img"></div><div class="col-xs-7 clear"><p class="text-center text-small clear"><b>'+ deals[x].merchant+'</b></p><span class="col-xs-12 text-center text-orange text-xlarge clear">$'+ deals[x].priceNow+'<span class="text-small cents">.'+deals[x].priceNowCents+'</span></span></div>', {maxWidth:150, minWidth:150});
		}
		return;
	},
	// doument.ready for browse map module
	didInsertElement: function(){
		var map = L.map('map'); // Create leaflet map
		var lat = 30.279739;
		var lon = -97.742699;
		var user; // User marker that represents user location

		// CLoudmade tile layer: http://{s}.tile.cloudmade.com/69d480cad34b43ada9b470555196029e/997/256/{z}/{x}/{y}.png
		// OSM tile layer: http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png
		L.tileLayer('http://{s}.tile.cloudmade.com/69d480cad34b43ada9b470555196029e/997/256/{z}/{x}/{y}.png', {
			attribution: '',
			maxZoom: 18,
			minZoom: 4
		}).addTo(map);

		navigator.geolocation.getCurrentPosition(startMapSuccess, onGeoError); // On start, add location
		navigator.geolocation.watchPosition(onGeoSuccess,onGeoError,{timeout: 3000 }); // Update location every 3 seconds4
		// startMapSuccess handles on map load, find user position
		function startMapSuccess(position){
			var lat = position.coords.latitude;
			var lon = position.coords.longitude;
			var userIcon = L.icon({
			    iconUrl: 'img/userMap.png',
			    iconSize:     [26, 75], // size of the icon
			    iconAnchor:   [13, 75], // point of the icon which will correspond to marker's location
			    shadowAnchor: [4, 62],  // the same for the shadow
			    popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
			});
      		user = L.marker([lat, lon], {icon: userIcon}).addTo(map);
      		map.setView(new L.LatLng(lat, lon),14); // Set view to location with zoom 14
		};
		// onGeoSuccess called everytime position changes
		function onGeoSuccess(position){
			var lat = position.coords.latitude;
			var lon = position.coords.longitude;
      		user.setLatLng([lat,lon]).update();	
		};
		// onGeoError called when cordova geolocation cannot find user location
		function onGeoError(){
			console.log("error with location");
			map.setView(new L.LatLng(lat, lon),14); // Set view to austin area with zoom 14
		};
		map.setView(new L.LatLng(lat, lon),14); // Set view to location with zoom 14
		this.addMap(map);
	}
});


/***************************************
	More Info Module
***************************************/
App.MoreinfoRoute = Ember.Route.extend({
	model: function(params){
		return deals.findBy('deal_id', params.deal_id);
	}
});

App.MoreinfoController = Ember.ObjectController.extend({
	isMapOpen: false, // True if map is open
	dealsGreen: function(){
		var num = parseInt(this.get('remaining'));
		if(num > 10){
			return true;
		}
		return false;
	}.property('remaining'),
	dealsYellow: function(){
		var num = parseInt(this.get('remaining'));
		if( (num <= 10) && (num >= 5) ){
			return true;
		}
		return false;
	}.property('remaining'),
});

App.MoreinfoView =  Ember.View.extend({
	touchListeners: function(self){
		// Event handler for "map icon" touch
		$('.map-icon').hammer().on('touch', function(event){
			var isMap = !self.get('controller.isMapOpen');
			self.set('controller.isMapOpen', isMap);
			// If map is open, hide image and show map, else reverse
			if(isMap){
				$('.deal-tile-img').hide();
				$('.moreinfo-map').show();
				$('#map-icon').attr('src', "img/icon/map_w.png");
			}
			else{
				$('.deal-tile-img').show();
				$('.moreinfo-map').hide();
				$('#map-icon').attr('src', "img/icon/map.png");
			}
		});
		// Event handler for "fav icon" touch
		$('#fav-btn').hammer().on('touch', function(event){
			var deal = self.get('controller.model');
			var fav = Ember.get(deal, 'favorite');
			if(!fav){
				$(this).attr("src",'img/icon/favorite_w.png');
			}
			else{
				$(this).attr("src",'img/icon/favorite.png');
			}
			Ember.set(deal, 'favorite', !fav);
			// Send favorite via socket io
			socket_sendFav({id: Ember.get(deal, 'deal_id'), fav: !fav});
		});
		// Event handler for "buy btn" touch
		$('#buy-btn').hammer().on('touch', function(event){
			var thing = $(this);
			window.location = 'file://' + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + window.location.pathname + '#/purchase/' + thing.attr('deal_id');
		});
		// Event handler for "back icon" navigation touch
		$('.back-icon').hammer().on('touch release', function(event){
			var thing = $(this).children('.icon');
			switch(event.type){
				case 'touch':
					thing.attr('src', "img/icon/back_w.png");
					break;
				case 'release':
					self.get('controller').transitionToRoute('browse');
					thing.attr('src', "img/icon/back.png");
			}
		});
	},
	// updateIcons() updates icons on document.ready
	updateIcons: function(self){
		if(self.get('controller.model.favorite')){
			$('#fav-btn').attr("src",'img/icon/favorite_w.png');
		}
	},
	// document.ready for more info module
	didInsertElement: function(){
		$('span.stars-beige').stars();
		var map = L.map('map');
		var lat = this.get('controller.model.location.lat');
		var lon = this.get('controller.model.location.lon');
		L.tileLayer('http://{s}.tile.cloudmade.com/69d480cad34b43ada9b470555196029e/997/256/{z}/{x}/{y}.png', {
			attribution: '',
			maxZoom: 18,
			minZoom: 4
		}).addTo(map);
		var marker = new L.Marker([lat, lon]).addTo(map);
		map.setView(new L.LatLng(lat, lon),14);
		$('.moreinfo-map').hide();
		this.updateIcons(this);
		this.touchListeners(this);
	}
});

/***************************************
	Purchase Module
***************************************/
App.PurchaseView = Ember.View.extend({
	// setTimer updates timer to current time
	setTimer: function(){
	    // Get reference to the interval doing the countdown
	    var now = new Date();
	    var end = new Date(bidwindow);
	   	var duration = parseInt((end - now)/1000) + 2;
	    var temp = 0;
	    var days = 0, hours = 0, minutes = 0, seconds = 0;
	    var time = duration;
	    // Set Time 
	    if(time >= 0){
		    temp = time;
	    	days = parseInt( (temp / 86400),10);
	    	temp %= 86400;
	    	hours = parseInt( (temp / 3600),10);
	    	temp %= 3600;
	    	minutes = parseInt( (temp / 60 ) );
	    	temp %= 60;
	    	seconds = temp;
	    	if(days == 0){
	    		$('#timer').removeClass('text-large');
		        $('#timer').addClass('text-xxlarge');
		        $('.timer-span').css({'height':$('html').height()*.06, 'top':$('html').height()*0.015});
		    }
	        $('#timer').text(((days == 0)?'': (days == 1)?'1 day ': days + ' days ') + ((hours<10)?'0':'') + hours + ':' + ((minutes<10)?'0':'') + minutes + ':' + ((seconds<10)?'0':'') + seconds);
		}
	},
	// touchListeners for purchase view
	touchListeners: function(self){
		// Touch listener for adding new credit card
		$('.addcard-toggle').hammer().on('tap', function(event){
			var flag = !self.get('controller.addCard');
			self.set('controller.addCard', flag);
		});

		// Select card radio functionality
		$('.card-selector').hammer().on('tap', function(event){
			var selected = $(this).hasClass('card-selected');
			$('.card-selector').each(function(){
				$('.card-selector').removeClass('card-selected');
			});
			if(selected){
				$(this).removeClass('card-selected');
			}
			else{
				$(this).addClass('card-selected');
			}
		});

		// Touch listener for purchase button
		$('#purchase-trigger').hammer().on('touch release', function(event){
			switch(event.type){
				case 'touch':
					// Animate change in purchase color
					$(this).children('h3').removeClass('text-orange');
					break;
				case 'release':
					// Declare variables
					var thing = self.get('controller');
					var userId = thing.get('currentUser.id');
					var cardId = undefined;
					$('#purchase-trigger').html('<img src="img/loader.gif" style="width: 200%; position: relative; left: -60%; margin-top: 3%" />');
					if (thing.get('noCards') || thing.get('addCard')) {
						var cardData = {
							number: thing.get('newCardNum'),
							exp_year: $('#year-select').val(),
							exp_month: $('#month-select').val(),
							cvc: thing.get('newCardCSC')
						};
						console.log(cardData);

						Stripe.setPublishableKey(stripeKey);
						Stripe.card.createToken(cardData, function(status, response) {
							if (response.error) {
								console.log(err);
								alert(response.error.message);
								errorOnPurchase();
							} else {
								var cardToken = response.id;
								$.post(server + "/users/" + userId + "/cards", {'token': cardToken})
									.done(function (data) {
										console.log(data);
										cardData.id = data.id;
										Cards.push(cardData);

										cardId = data.id;

										console.log("new card added, now send this over");
										console.log(cardData);
										thing.set('addCard', false);

										var purchaseData = {
											"user_id": userId,
											"card_id": cardId,
											"query_id": searchQuery,
											"deal_id": thing.get('model.deal_id'),
											"price": thing.get('model.price_now'),
											"deal": thing.get('model')
										};
										console.log(purchaseData);

										$.post(server + "/users/" + userId + "/debit", purchaseData)
											.done(function (data) {
												console.log(data);
												gotVoucher(data);
											})
											.fail(function (err) {
												alert(err.responseText);
												errorOnPurchase();
											});
									})
									.fail(function (err) {
										alert(err.responseText);
										errorOnPurchase();
									});
							}
						});
					} else {
						var selected = $('.card-selected');
						
						if (selected.length > 0) {
							cardId = parseInt(selected.attr('card_id'));

							var purchaseData = {
								"user_id": userId,
								"card_id": cardId,
								"query_id": searchQuery,
								"deal_id": thing.get('model.deal_id'),
								"price": thing.get('model.price_now'),
								"deal": thing.get('model')
							};
							console.log(purchaseData);

							$.post(server + "/users/" + userId + "/debit", purchaseData)
								.done(function (data) {
									console.log(data);
									gotVoucher(data);
								})
								.fail(function (err) {
									alert(err.responseText);
									errorOnPurchase();
								});
						} else {
							alert('need to have a card selected');
							errorOnPurchase();
						}
					}
					break;
			}
			// gotVoucher() method is called on successful purchase
			function gotVoucher(data){
				var thing = self.get('controller');
				resetBrowseData();
				purchased_deal = data;
				purchased_deal["priceNow"] = Math.floor(Ember.get(purchased_deal,'price_now'));
        		purchased_deal["priceNowCents"] = leading_zero(Math.round(Ember.get(purchased_deal,'price_now')*100)%100);
				thing.transitionToRoute('feedback');
			};

			// errorOnPurchase() method is called on purchase error, resets DOM
			function errorOnPurchase(){
				$('#purchase-trigger').html('<h3 class="text-orange text-center voffset5 height95"><strong>PURCHASE</strong></h3>');
			}
		});

	},
	// document.ready for purchase state
	didInsertElement: function(){
		$('#year-select').html(make_year_options(13, 1));
		$('.deal-tile-img').css('height', $('.contain').height() * 0.2 +'px');
		this.setTimer();
		this.touchListeners(this);
	}
});
App.PurchaseRoute = Ember.Route.extend({
	model: function(params){
		console.log(params);
		return deals.findBy('deal_id', params.deal_id);
	},
});
App.PurchaseController = Ember.ObjectController.extend({
	needs: ['application'],
	currentUser: Ember.computed.alias('controllers.application.currentUser'),
	noCards: function(){
		return Cards.length == 0;
	}.property('@each.Cards'),
	addCard: false,
	cards: function(){
		return Cards;
	}.property('@each.Cards'),
	totalprice: function(){
		return this.get('model.price_now').toFixed(2);
	}.property('price_now'),
});

/***************************************
	My Profile Module
***************************************/
App.MyprofileView = Ember.View.extend({
	// touch listeners on document.ready
	touchListeners: function(self){
		// Event handler for save button
		$('#save-btn').hammer().on('touch', function(event){
			// Declare variables
			var user;
			var thing = self.get('controller');
			var sex = $('.checked').children('input').attr("value") || "";
			var dob = new Date(thing.dateYear, thing.dateMonth, thing.dateDay);

			self.set('controller.currentUser.birthdate', rfc3339_dt(dob));
			self.set('controller.currentUser.sex', sex);
			user = self.get('controller.currentUser');
			alert('Saved');
			$.ajax({
			  url: server + "/users",
			  type: 'PUT',
			  data: user,
			  success: function(data) {
			    console.log("SUCCESS");
				console.log(data);
			  },
			  error: function(data, statusCode) {
			    console.log("ERROR");
			  }
			});
		});
	},
	// datePicker for my profile birthday
	DatePicker: function(self){
		// Set Date Variables
		var today = new Date();
		var month = today.getMonth();
		var date = today.getDate();
		var year = today.getFullYear();
		var dob = self.get('controller.currentUser.birthdate');

		// Get Elements from DOM
		var search_date = $('#birth-date');
		var date_picker = $('#datePicker-contain');
		var set_btn = $('#setTime');
		var cancel_btn = $('#cancelTime');
		var add_month = $('#addMonth');
		var add_day = $('#addDay');
		var add_year = $('#addYear');
		var curr_month = $('#currMonth');
		var curr_day = $('#currDay');
		var curr_year = $('#currYear');
		var sub_month = $('#subMonth');
		var sub_day = $('#subDay');
		var sub_year = $('#subYear');

		var open = false;
		var hold_interval;
		var this_month = month;
		var this_day = date;
		var this_year = year;

		var months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]; // Month by abreviation
		var days = [31,28,31,30,31,30,31,31,30,31,30,31]; // Num of days in each month

		if(dob != null){
			var date = new Date(dob);
			month = date.getMonth();
			year = date.getFullYear();
			date = date.getDate()+1;
			this_month = month;
			this_day = date;
			this_year = year;	
		}
		self.set('controller.dateYear', year);
		self.set('controller.dateMonth', month);
		self.set('controller.dateDay', date);
		search_date.val(months[month] +' '+ date +', '+ year); // Set input to today's Date
		// Set DatePicker to today's date
		curr_month.text(months[this_month]);
		curr_day.text(this_day);
		curr_year.text(this_year);
		date_picker.hide();

		// Listener for user opening DatePicker
		search_date.hammer().on('tap', function(event){
			if(!open){
				date_picker.show(); // Show datepicker
				open = true; // Open datepicker
				setTimeout(function(){
					date_picker.css('opacity', '1'); // Set opacity
				}, 50);
			}
		});
		function onTouch(self){
			self.addClass('bg-light-gray');
			self.removeClass('bg-beige');
		};
		function offTouch(self){
			self.addClass('bg-beige');
			self.removeClass('bg-light-gray');
		};
		function clearInt(){
			window.clearInterval(hold_interval);
		}
		function increaseMonth(){
			this_month++;
			if(this_month == months.length){
				this_month = 0; // Set to JAN if DEC
			}
			if(this_day > days[this_month]){
				this_day = days[this_month];
				curr_day.text(this_day);
			}
			curr_month.text(months[this_month]);
		};
		function decreaseMonth(){
			this_month--;
			if(this_month < 0){
				this_month = months.length-1; // Set to JAN if DEC
			}
			if(this_day > days[this_month]){
				this_day = days[this_month];
				curr_day.text(this_day);
			}
			curr_month.text(months[this_month]);
		};
		function increaseDay(){
			$(this).removeClass('bg-beige');
			this_day++;
			if(this_day > days[this_month]){
				this_day = 1; // Set to JAN if DEC
			}
			curr_day.text(this_day);
		};
		function decreaseDay(){
			this_day--;
			if(this_day < 1){
				this_day = days[this_month]; // Set to JAN if DEC
			}
			curr_day.text(this_day);
		};
		function increaseYear(){
			this_year++;
			if((this_year % 4) == 0){
				days[1] = 29;
			}
			else{
				days[1] = 28;
				if(this_day > days[this_month]){
					this_day = days[this_month];
					curr_day.text(this_day);
				}
			}
			curr_year.text(this_year);
		};
		function decreaseYear(){
			if(this_year > 0){
				this_year--;
				if((this_year % 4) == 0){
					days[1] = 29;
				}
				else{
					days[1] = 28;
					if(this_day > days[this_month]){
						this_day = days[this_month];
						curr_day.text(this_day);
					}
				}
				curr_year.text(this_year);
			}
		};
		// Listeners for changing month
		add_month.add(sub_month).hammer({hold_threshold: 2, hold_timeout: 250}).on('touch hold release', function(event){
			var thing = $(this);
			var id = event.target.id;
			switch(event.type){
				case 'hold':
					hold_interval = setInterval(function(){
						if(id == 'addMonth')increaseMonth();
						else decreaseMonth();
					}, 200);
					break;
				case 'touch':
					onTouch(thing);
					if(id == 'addMonth')increaseMonth();
					else decreaseMonth();
					break;
				case 'release':
					offTouch(thing);
					clearInt();
					break;
			}
		});

		// Listeners for changing day
		add_day.add(sub_day).hammer({hold_threshold: 2, hold_timeout: 250}).on('touch hold release', function(event){
			var thing = $(this);
			var id = event.target.id;
			switch(event.type){
				case 'hold':
					hold_interval = setInterval(function(){
						if(id == 'addDay')increaseDay();
						else decreaseDay();
					}, 100);
					break;
				case 'touch':
					onTouch(thing);
					if(id == 'addDay')increaseDay();
					else decreaseDay();
					break;
				case 'release':
					offTouch(thing);
					clearInt();
					break;
			}
		});

		// Listeners for changing year
		add_year.add(sub_year).hammer({hold_threshold: 2, hold_timeout: 250}).on('touch hold release', function(event){
			var thing = $(this);
			var id = event.target.id;
			switch(event.type){
				case 'hold':
					hold_interval = setInterval(function(){
						if(id == 'addYear')increaseYear();
						else decreaseYear();
					}, 100);
					break;
				case 'touch':
					onTouch(thing);
					if(id == 'addYear')increaseYear();
					else decreaseYear();
					break;
				case 'release':
					offTouch(thing);
					clearInt();
					break;
			}
		});

		// Listener for set button
		set_btn.hammer().on('touch', function(event){
			self.set('controller.dateYear', this_year);
			self.set('controller.dateMonth', this_month);
			self.set('controller.dateDay', this_day);
			search_date.val(months[this_month] +' '+ this_day +', '+ this_year); // Set input to Date
			// Close date picker
			date_picker.css('opacity', '0');
			setTimeout(function(){
				date_picker.hide();
				open = false;
			}, 500);
		});

		// Listener for cancel button
		cancel_btn.hammer().on('touch', function(event){
			// Close date picker
			date_picker.css('opacity', '0');
			setTimeout(function(){
				date_picker.hide();
				open = false;
				// Reset variables + input
				var string = search_date.val().replace(',', '').split(" ");
				this_month = months.indexOf(string[0]);
				this_day = string[1];
				this_year = string[2];
				curr_month.text(months[this_month]);
				curr_day.text(this_day);
				curr_year.text(this_year);
			}, 500);
		});

	},
	// Updates profile gender radios
	updateProfile: function(self){
		var gender = self.get('controller.currentUser.sex');
		if(gender != null){
			if(gender == 'M'){
				$('#male-radio').addClass('checked');
			}
			else if(gender == 'F'){
				$('#female-radio').addClass('checked');
			}
		}
	},
	// document.ready for my profile module
	didInsertElement: function(){
		$('#year-select').html(make_year_options(150, -1));
		this.updateProfile(this);
		this.DatePicker(this);
		this.touchListeners(this);
	}
});
App.MyprofileController = Ember.ObjectController.extend({
	needs: ['application'],
	currentUser: Ember.computed.alias('controllers.application.currentUser'),
	dateYear: '',
	dateMonth: '',
	dateDay: ''
});

/***************************************
	Voucher Module
***************************************/
App.VoucherRoute = Ember.Route.extend({
	model: function(){
		return purchased_deal;
	},
	setupController: function(controller, model) {
		controller.set('model', model);
	}
});
App.VoucherView = Ember.View.extend({
	// Touch listeners for voucher document.ready
	touchListeners: function(self){
		// Event handler for "map icon"
		$('.map-icon').hammer().on('touch', function(event){
			var isMap = !self.get('controller.isMapOpen');
			self.set('controller.isMapOpen', isMap);
			// If map is open, hide image and show map, else reverse
			if(isMap){
				$('.deal-tile-img').hide();
				$('.moreinfo-map').show();
				$('#map-icon').attr('src', "img/icon/map_w.png");
			}
			else{
				$('.deal-tile-img').show();
				$('.moreinfo-map').hide();
				$('#map-icon').attr('src', "img/icon/map.png");
			}
		});
		// Event handler for "back icon"
		$('#back-icon').hammer().on('touch release', function(event){
			switch(event.type){
				case 'touch':
					$(this).attr("src",'img/icon/back_w.png');
					break;
				case 'release':
					$(this).attr("src",'img/icon/back.png');
					setTimeout(function(){
						window.location = ('file://' + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + window.location.pathname + '#/mypurchases');
					}, 100)
					break;
			}
		});
		// Event handler for "forward icon"
		$('#forward-icon').hammer().on('touch release', function(event){
			switch(event.type){
				case 'touch':
					$(this).attr("src",'img/icon/forward_w.png');
					break;
				case 'release':
					$(this).attr("src",'img/icon/forward.png');
					window.location = ('file://' + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + window.location.pathname + '#/search');
					break;
			}
		});
	},
	// document.ready for voucher module
	didInsertElement: function(){
		var map = L.map('map');
		var lat = this.get('controller.model.location.lat');
		var lon = this.get('controller.model.location.lon');
		L.tileLayer('http://{s}.tile.cloudmade.com/69d480cad34b43ada9b470555196029e/997/256/{z}/{x}/{y}.png', {
			attribution: '',
			maxZoom: 18,
			minZoom: 4
		}).addTo(map);
		var marker = new L.Marker([lat, lon]).addTo(map);
		map.setView(new L.LatLng(lat, lon),14);
		$('.moreinfo-map').hide();
		// Update prev state
		if( (App.get('currentPath') == "voucher") && (App.get('previousPath') == "feedback") ){
			$('#voucher-footer-icon').html('<img class="hoffset30 fith100 icon" id="forward-icon" src="img/icon/forward.png"/>');
		}
		else{
			$('#voucher-footer-icon').html('<img class="hoffset30 fith100 icon" id="back-icon" src="img/icon/back.png"/>');
		}
		this.touchListeners(this);
	}
});
App.VoucherController = Ember.ObjectController.extend({
	isMapOpen: false, // True if map is open
	afterPurchaseState: '',
	isVoucherImg: function(){
		return this.get('voucher_img') != null;
	}.property('voucher_img'),
	dealImg: function(){
		var url = "img/food/" + this.get('img');
		return url;
	}.property('img')
});

/***************************************
	My Purchases Module
***************************************/
App.MypurchasesRoute = Ember.Route.extend({
	model: function(){
		return purchases;
	},
});
App.MypurchasesController = Ember.ArrayController.extend({
	needs: ['application'],
	currentUser: Ember.computed.alias('controllers.application.currentUser'),
	noPurchases: function(){
		return this.get('model').length == 0;
	}.property('@each'),
	loadingPurchases: true
});
App.MypurchasesView = Ember.View.extend({
	touchListeners: function(self){
		// Event handler for my purchase deal tile
		$('.deal-tile').hammer().on('tap', function(event){
			purchased_deal = purchases.findBy('deal_id', $(this).attr('purchase_id'));
			self.get('controller').transitionToRoute('voucher');
		});
	},
	// loadPurchases() method loads purchases from server
	loadPurchases: function(self){
		var id = self.get('controller.currentUser.id');
		console.log(id);
		setTimeout(function(){
			var posted = $.get(server + "/purchases/" + id);
			posted.done(function(data){
				console.log("YES");
				console.log(data);
				purchases = data;
				self.set('controller.model', purchases);
				self.set('controller.loadingPurchases', false);
				for(var x = 0; x<purchases.length; x++){
			        purchases[x]["priceNow"] = Math.floor(Ember.get(purchases[x],'price_now'));
			        purchases[x]["priceNowCents"] = leading_zero(Math.round(Ember.get(purchases[x],'price_now')*100)%100);
			    }
				setTimeout(function(){
					self.touchListeners(self);
				}, 200);
			});
			posted.fail(function(res) {
				self.set('controller.loadingPurchases', false);
				if(purchases.length > 0){
					setTimeout(function(){
						self.touchListeners(self);
					}, 200);
				}
			});
		}, 500);
	},
	didInsertElement: function(){
		this.loadPurchases(this);
	}
});

/***************************************
	Feedback Module
***************************************/
App.FeedbackView = Ember.View.extend({
	touchListeners: function(self){
		// Event handler for feedback trigger
		$('#feedback-trigger').hammer().on('touch release', function(event){
			switch(event.type){
				case 'touch':
					$(this).children().attr("src",'img/icon/forward_w.png');
					break;
				case 'release':
					$(this).children().attr("src",'img/icon/forward.png');
					var id = self.get('controller.currentUser.id');
					var obj = {
						"user_id": id,
						"positive_comment": $('#feedback-positive').val(),
						"negative_comment": $('#feedback-negative').val(),
						"other_comment": $('#feedback-other').val()
					}
					var id = self.get('controller.currentUser.id');
					var posted = $.post(server + "/feedback", obj);
					posted.done(function(data){
						console.log("YES");
						setTimeout(function(){
							self.get('controller').transitionToRoute('voucher');
						},200);
					});
					posted.fail(function(res) {
						console.log("NO");
					});
					
			}
		});
	},
	didInsertElement: function(){
		this.touchListeners(this);
	}
});

App.FeedbackController = Ember.Controller.extend({
	needs: ['application'],
	currentUser: Ember.computed.alias('controllers.application.currentUser'),
});
